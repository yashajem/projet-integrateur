﻿using System;

[Serializable]
public class Leaderboard
{
    public string login;
    public int best_score;
}