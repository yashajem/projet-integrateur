using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using Mirror;
using Telepathy;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public struct ServerMessage : NetworkMessage
{
    public bool startQnA;
    public bool startQnARapid;
    public CLQuestion q;
    public CLQuestionRapid qR;
    public bool desactiverTer;
    public Territory currentTer;
    public bool draw;
    public GamePhase phase;
    public bool fin;
    public bool winner;
}

public enum GamePhase
{
    baseSelect_1 = 1,
    expantion_21 = 21,
    allocation_22 = 23,
    battle_3 = 3,
    end_4 = 4
}

/*----------------------------------------------------------------------------------------------------------------*/
/* [WARNING] : Toutes les fonction suivantes vont s'executer QUE sur le serveur.
               Normalement, les joueurs n'ont pas acces  */
/*----------------------------------------------------------------------------------------------------------------*/


public class GameManager : NetworkBehaviour
{
    public static GameManager instance;
    
    private bool _notExecuted = true;
    string json;
    string jsonRapid;

    private bool phase1Fin = false;
    private bool phase2Fin = false;
    private bool phase3Fin = false;
    private bool phase4Fin = false;

    public Questions questionsJson;
    public QuestionsRapidite questionsRapiditeJson;

    private Territory t;
    
    public int terDispo;
    public int roundsPhase3;
    public int roundsPhase2;
    
    Player winner = null;


    /* Liste des territoires disponibles dans un match. */
    public List<Territory> territoriesList;
    
    /* Une liste synchronizé avec le serveur des joueurs.
     Ici dès qu'il y a un changement, et le serv et le client vont savoir */
    public readonly SyncList<Player> players = new SyncList<Player>();

    public List<Player> playersWithBase = new List<Player>();
    
    public readonly SyncList<Player> playersThatPlay = new SyncList<Player>();

    public readonly SyncList<Player> playersThatAnswered = new SyncList<Player>();

    private readonly List<string> stringEvenement = new();

    /* Une liste des couleurs pour les joueurs */
    private readonly List<Color> _colorList = new()
    {
        Color.Lerp(Color.red, new Color(1f, 0.5f, 0f), 0.5f), // reddish-orange
        Color.Lerp(Color.blue, new Color(0f, 0.5f, 1f), 0.5f), // bluish-cyan
        Color.Lerp(Color.green, new Color(0f, 1f, 0.5f), 0.5f), // greenish-cyan
        Color.Lerp(new Color(1f, 0f, 0.5f), new Color(1f, 0f, 1f), 0.5f), // pinkish-purple
        Color.Lerp(new Color(1f, 0.5f, 0f), new Color(1f, 1f, 0f), 0.5f), // orangeish-yellow
        Color.Lerp(Color.yellow, new Color(1f, 1f, 0.5f), 0.5f), // pale-yellow
        Color.Lerp(new Color(0.5f, 0f, 1f), new Color(1f, 0f, 1f), 0.5f), // purpleish-magenta
        Color.Lerp(Color.cyan, new Color(0f, 1f, 1f), 0.5f), // pale-cyan
        Color.green,
        Color.Lerp(Color.red, Color.yellow, 0.5f) //brown
    };

    /* Classe qui va contenir toutes les fonctions responsables de la comm avec BDD */
    public ServerSideScripts _serverSideScripts;
    
    /*Variable qui va contenir un index random pour les questions*/
    public int randomNr;
    
    /*L'etat(phase) de jeu*/
    public GamePhase gameStatus;

    /*L'id de partie*/
    public int gameID;
    
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else instance = this;
        Debug.Log("[SERVER] GameManager initialized");
    }
    
    private void Start()
    {
        //Vu que dans la BDD c'est des INT, on fais juste un numero random
        gameID = Random.Range(0, 100000);
        
        //Initialisation de compteur de territoires disponibles a capturer
        terDispo = 0;
        
        //Initialisation des booleans qui sont responsables pour le passage entre les phases 
        phase2Fin = false;
        phase1Fin = false;
        phase3Fin = false;

        /* Mettre en place un handler qui va executer la methode OnReceive dès qu'on a un message provenant du client */
        NetworkServer.RegisterHandler<ClientMessage>(OnReceiveClient);
        NetworkServer.RegisterHandler<TerritoryMessage>(OnReceiveTerritory);
        
        /* Initialiser la classe ServerSideScripts */
        _serverSideScripts = new ServerSideScripts();

        /*Des coroutines pour recuperer les questions de la BDD*/
        StartCoroutine(GetJSON());
        StartCoroutine(GetJSONRapid());
        
        //Dès que le jeu start, c'est la phase de selection de base
        gameStatus = GamePhase.baseSelect_1;
        foreach (var p in players)
        {
            p.phase = gameStatus;
        }
        
        //On va remplir un tableau avec les ID des participants
        int[] idParticipant = new int[players.Count];
        
        for (int i = 0; i < players.Count; i++)
        {
            idParticipant[i] = players[i].ID;
        }

        //Et ensuite on va l'envoyer vers la BDD pour stocker les données
        StartCoroutine(_serverSideScripts.SendDataToServer(idParticipant, gameID));
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  La fonction OnReceive va gerer tous les messages provenant d'un client. Il va recevoir une structure
                    ClientMessage qui contient toutes les information qu'il a besoin. Regarder "Player.cs" pour voir
                    la structure ClientMessage                                                                        */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void OnReceiveClient(NetworkConnectionToClient conn, ClientMessage msg)
    {
        Debug.Log("[SERVER] Received : " + msg.answer +  " from : " + msg.p.playerName + " " +
                  msg.P_currentTerritory + " " + msg.p.canAnswer);
        Debug.Log($"[RAPID] {msg.type}");
        
        /*Si le joueur a répondu a une question de rapidité pendant la phase de bataille,
         on doit valider la réponse et lui dire si c'est bien ou pas*/
        if (msg is {type: "rapid", answered: true} && gameStatus == GamePhase.battle_3)
        {
            Debug.Log($"[RAPID] Received {msg.answer} from {msg.p.playerName} his time of response was " +
                      $"{msg.p.timeLeft}. " +
                      $"The correct answer is {questionsRapiditeJson.questions[randomNr].correctAnswer}");
            
            msg.p.P_answer = msg.answer;
            
            /*On ajoute le joueur qui vient d'envoyer un message dans la liste des joueurs qui ont répondu.*/
            playersThatAnswered.Add(msg.p);

            /*Tant qu'il n'y a pas tous les joueurs nécessaires qui ont répondu dans la liste, on va pas continuer*/
            if (playersThatAnswered.Count != 2)
            {
                Debug.Log("[RAPID] Not everybody answered");
            }
            else
            {
                /*Sinon, commencons la validation des réponses*/
                
                foreach (var player in playersThatAnswered)
                {
                    player.questionCounter++;
                }

                foreach (var player in players)
                {
                    player.shouldHaveResponded = false;
                }
                
                /*Si le joueur envoie rien (timer = 0) on va mettre sa réponse a 0 pour que int parse fonctionne*/
                if (string.IsNullOrEmpty(playersThatAnswered[0].P_answer))
                {
                    playersThatAnswered[0].P_answer = "0";
                }
                
                if (string.IsNullOrEmpty(playersThatAnswered[1].P_answer))
                {
                    playersThatAnswered[1].P_answer = "0";
                }
                
                /*On récupere les infos nécessaires pour valider la réponse, on transforme des string dans des int*/
                int answer_p1 = int.Parse(playersThatAnswered[0].P_answer);
                int answer_p2 = int.Parse(playersThatAnswered[1].P_answer);
                /*Récuperons le temps restant qu'ils ont*/
                float timeLeft_p1 = playersThatAnswered[0].timeLeft;
                float timeLeft_p2 = playersThatAnswered[1].timeLeft;
                
                /*Et biensur la bonne réponse*/
                int correctAnswer = int.Parse(questionsRapiditeJson.questions[randomNr].correctAnswer);

                /*Calculons la difference de temps pour savoir qui a répondu le plus vite*/
                float diff_p1 = 20.0f - timeLeft_p1;
                float diff_p2 = 20.0f - timeLeft_p2;

                Debug.Log(timeLeft_p1);
                Debug.Log(timeLeft_p2);
                
                Debug.Log(diff_p1);
                Debug.Log(diff_p2);
                
                Debug.Log(answer_p1);
                Debug.Log(answer_p2);
                Debug.Log(correctAnswer);
                
                //correctAnswer - answer_1 et correctAnswer - answer_2
                //on prend le plus petit 

                /*Vu que c'est au plus proche, on calcule le résultat qui doit etre positif*/
                int result_p1 = Mathf.Abs(correctAnswer - answer_p1);
                int result_p2 = Mathf.Abs(correctAnswer - answer_p2);

                Debug.Log(result_p1);
                Debug.Log(result_p2);

                //Si le joueur 1 a donné la bonne réponse
                if (answer_p1 == correctAnswer)
                {
                    playersThatAnswered[0].hadCorrectAnswer = true;
                    playersThatAnswered[0].shouldHaveResponded = true;
                    playersThatAnswered[1].hadCorrectAnswer = false;
                    playersThatAnswered[1].shouldHaveResponded = true;

                    playersThatAnswered[0].corretQuestionCounter++;

                    if (t.isFortified)
                    {
                        t.isFortified = false;
                        ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //Vider la liste des joueurs
                        playersThatAnswered.Clear();
                        return;
                    }
                    
                    //p1 won
                    Debug.Log($"{playersThatAnswered[0].playerName} responded spot on! He wins!");
                    //On ajoute l'evenement dans la liste des strings d'evenements
                    stringEvenement.Add($"{playersThatAnswered[0].playerName} responded spot on! He wins!");
                    //Si le joueur qui vient de gagner, ne contient pas déjà le territoire, on va lui
                    //attribuer les points et le territoire, et faire toutes les operations nécessaires
                    if (!playersThatAnswered[0].territoryList.Contains(t))
                    {
                        Debug.Log("Passed the if in player2 won if resp1 == resp2");
                        //On va changer la couleur du territoire qu'il vient de gagner
                        ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                        //On va ajouter le territoire dans sa liste des territoires
                        playersThatAnswered[0].territoryList.Add(t);
                        //On va ajouter les points
                        playersThatAnswered[0].points += 400;
                        //Pour le joueur qui a perdu le territoire on va enlever des points
                        playersThatAnswered[1].points -= 400;
                        //On va update les voisins du joueur qui viens de gagner
                        ServerUpdateNeighbours(playersThatAnswered[0]);
                        //Pour le joueur qui a perdu,
                        //on va supprimer le territoire qu'il a perdu, et on va update les voisins 
                        ServerDeleteAndUpdate(playersThatAnswered[1], t);
                        //Changement de tour
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        //Le serveur va envoyer un message au tous les joueurs dans sa liste des joueurs,
                        //disant que le QnA est fini
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //On vide la liste des joueurs qui ont répondu
                        playersThatAnswered.Clear();
                        return;
                    }
                    
                    //Si il contient déjà le territoire, ca veut dire qu'il a defendu son territoire
                    if (playersThatAnswered[0].territoryList.Contains(t))
                    {
                        Debug.Log($"Player {playersThatAnswered[0].playerName} " +
                                  $"already contains this territory! " +
                                  $"He defended with success from {playersThatAnswered[1].playerName}");
                        stringEvenement.Add($"Player {playersThatAnswered[0].playerName} " +
                                            $"already contains this territory! " +
                                            $"He defended with success from {playersThatAnswered[1].playerName}");
                        //On ajoute 100 points
                        playersThatAnswered[0].points += 100;
                        //Changement de tour
                        if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                        else ServerNextTurn(playersThatAnswered[0]);
                        //Envoyer le message a tous les joueurs pour finir le QnA
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //Vider la liste des joueurs
                        playersThatAnswered.Clear();
                        return;
                    }
                }

                //Exactement la meme procedure, mais si le joueur n2 gagne
                if (answer_p2 == correctAnswer)
                {
                    playersThatAnswered[1].corretQuestionCounter++;
                 
                    playersThatAnswered[1].hadCorrectAnswer = true;
                    playersThatAnswered[1].shouldHaveResponded = true;
                    playersThatAnswered[0].hadCorrectAnswer = false;
                    playersThatAnswered[0].shouldHaveResponded = true;

                    if (t.isFortified)
                    {
                        t.isFortified = false;
                        ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //Vider la liste des joueurs
                        playersThatAnswered.Clear();
                        return;
                    }

                    
                    //p2 won
                    Debug.Log($"{playersThatAnswered[1].playerName} responded spot on! He wins!");
                    stringEvenement.Add($"{playersThatAnswered[1].playerName} responded spot on! He wins!");
                    if (!playersThatAnswered[1].territoryList.Contains(t))
                    {
                        ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                        playersThatAnswered[1].territoryList.Add(t);
                        playersThatAnswered[1].points += 400;
                        playersThatAnswered[0].points -= 400;
                        ServerUpdateNeighbours(playersThatAnswered[1]);
                        ServerDeleteAndUpdate(playersThatAnswered[0], t);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        playersThatAnswered.Clear();
                        return;
                    }
                    
                    if (playersThatAnswered[1].territoryList.Contains(t))
                    {
                        Debug.Log($"Player {playersThatAnswered[1].playerName} " +
                                  $"already contains this territory!" +
                                  $"He defended with success from {playersThatAnswered[0].playerName}");
                        stringEvenement.Add($"Player {playersThatAnswered[1].playerName} " +
                                        $"already contains this territory!" +
                                        $"He defended with success from {playersThatAnswered[0].playerName}");
                        playersThatAnswered[1].points += 100;
                        if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                        else ServerNextTurn(playersThatAnswered[0]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        playersThatAnswered.Clear();
                        return;
                    }
                }
                
                //si l'ecart dans les réponses est le meme , on regarde la val de timer
                if (result_p1 == result_p2)
                {
                    //Si le joueur 2 a répondu plus vite que joueur 1
                    if (diff_p1 > diff_p2)
                    {
                        playersThatAnswered[1].hadCorrectAnswer = true;
                        playersThatAnswered[1].shouldHaveResponded = true;
                        playersThatAnswered[0].hadCorrectAnswer = false;
                        playersThatAnswered[0].shouldHaveResponded = true;
                        
                        playersThatAnswered[1].corretQuestionCounter++;
                        
                        if (t.isFortified)
                        {
                            t.isFortified = false;
                            ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                            if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                            else ServerNextTurn(playersThatAnswered[1]);
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            //Vider la liste des joueurs
                            playersThatAnswered.Clear();
                            return;
                        }

                        Debug.Log("Player 2 won");
                        Debug.Log("Territory : " + t);
                        Debug.Log($"{playersThatAnswered[1].playerName} " +
                                  $"defeated {playersThatAnswered[0].playerName}");
                        stringEvenement.Add($"{playersThatAnswered[1].playerName} " +
                                            $"defeated {playersThatAnswered[0].playerName}");

                        //Pour le joueur 2, exactement la meme procedure qu'avant
                        //Si il contient pas ce territoire, il va le prendre
                        if (!playersThatAnswered[1].territoryList.Contains(t))
                        {
                            Debug.Log("Passed the if in player2 won if resp1 == resp2");
                            //Changement de couleur du territoire capturé
                            ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                            //Ajout de territoire dans la liste des territoires du joueur
                            playersThatAnswered[1].territoryList.Add(t);
                            //Ajout des points                            
                            playersThatAnswered[1].points += 400;
                            //On enleve des points pour le joueur perdant
                            playersThatAnswered[0].points -= 400;
                            //On update les voisins pour le gagnant
                            ServerUpdateNeighbours(playersThatAnswered[1]);
                            //Et on supprime le territoire et update les voisins pour le perdant
                            ServerDeleteAndUpdate(playersThatAnswered[0], t);
                            //Changement de tour
                            if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                            else ServerNextTurn(playersThatAnswered[1]);
                            //Et envoi de message a chaque joueur disant que le QnA est fini
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            //Vider la liste des joueurs qui ont répondu
                            playersThatAnswered.Clear();
                            return;
                        }
                        
                        //Si il contient déjà ce territoire, alors il c'est déféndu avec success
                        if (playersThatAnswered[1].territoryList.Contains(t))
                        {
                            Debug.Log($"Player {playersThatAnswered[1].playerName} " +
                                      $"already contains this territory!" +
                                      $"He defended with success form {playersThatAnswered[0].playerName}");
                            stringEvenement.Add($"Player {playersThatAnswered[1].playerName} " +
                                                $"already contains this territory!" +
                                                $"He defended with success form {playersThatAnswered[0].playerName}");
                            //On ajoute des points pour la défénce
                            playersThatAnswered[1].points += 100;
                            //Changement de tour
                            if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                            else ServerNextTurn(playersThatAnswered[0]);
                            //Le serveur va envoyer un message a tout le monde disant que le QnA est fini
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            //Vider la liste des joueurs qui ont répondu
                            playersThatAnswered.Clear();
                            return;
                        }
                    }
                    else
                    {
                        playersThatAnswered[0].corretQuestionCounter++;
                        
                        playersThatAnswered[0].hadCorrectAnswer = true;
                        playersThatAnswered[0].shouldHaveResponded = true;
                        playersThatAnswered[1].hadCorrectAnswer = false;
                        playersThatAnswered[1].shouldHaveResponded = true;


                        if (t.isFortified)
                        {
                            t.isFortified = false;
                            ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                            if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                            else ServerNextTurn(playersThatAnswered[1]);
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            //Vider la liste des joueurs
                            playersThatAnswered.Clear();
                            return;
                        }

                        
                        //Sinon c'est le premier qui a gagne, et ca suit la meme procedure qu'en haut 
                        Debug.Log("Player 1 won");
                        Debug.Log("Territory : " + t);
                        Debug.Log(playersThatAnswered[0].playerName + " " +
                                  "defeated " + playersThatAnswered[1].playerName);
                        stringEvenement.Add($"{playersThatAnswered[0].playerName} " +
                                            $"defeated  {playersThatAnswered[1].playerName}");
                        if (!playersThatAnswered[0].territoryList.Contains(t))
                        {
                            Debug.Log("Passed the if in player1 won in else");
                            ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                            playersThatAnswered[0].territoryList.Add(t);
                            playersThatAnswered[0].points += 400;
                            playersThatAnswered[1].points -= 400;
                            ServerUpdateNeighbours(playersThatAnswered[0]);
                            ServerDeleteAndUpdate(playersThatAnswered[1], t);
                            if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                            else ServerNextTurn(playersThatAnswered[0]);
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                        
                        if (playersThatAnswered[0].territoryList.Contains(t))
                        {
                            Debug.Log($"Player {playersThatAnswered[0].playerName} " +
                                      $"already contains this territory!" +
                                      $"He defended with success from {playersThatAnswered[1].playerName}");
                            stringEvenement.Add($"Player {playersThatAnswered[0].playerName} " +
                                                $"already contains this territory!" +
                                                $"He defended with success from {playersThatAnswered[1].playerName}");
                            playersThatAnswered[0].points += 100;
                            if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                            else ServerNextTurn(playersThatAnswered[0]);

                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnARapid = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                    }
                }
                
                //D'ici en bas, ca suit le memes procedures que avant
                if (result_p1 > result_p2 || diff_p2 > diff_p1)
                {
                    //player 2 gagne
                    Debug.Log("Player 2 won");
                    
                    playersThatAnswered[1].hadCorrectAnswer = true;
                    playersThatAnswered[1].shouldHaveResponded = true;
                    playersThatAnswered[0].hadCorrectAnswer = false;
                    playersThatAnswered[0].shouldHaveResponded = true;

                    
                    if (t.isFortified)
                    {
                        t.isFortified = false;
                        ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //Vider la liste des joueurs
                        playersThatAnswered.Clear();
                        return;
                    }

                    
                    playersThatAnswered[1].corretQuestionCounter++;
                    
                    Debug.Log(playersThatAnswered[1].playerName + " defeated " + playersThatAnswered[0].playerName);
                    stringEvenement.Add($"{playersThatAnswered[1].playerName} defeated {playersThatAnswered[0].playerName}");

                    if (!playersThatAnswered[1].territoryList.Contains(t))
                    {
                        Debug.Log("passed the if in resp1 > resp2");
                        ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                        playersThatAnswered[1].territoryList.Add(t);
                        playersThatAnswered[1].points += 400;
                        playersThatAnswered[0].points -= 400;
                        ServerUpdateNeighbours(playersThatAnswered[1]);
                        ServerDeleteAndUpdate(playersThatAnswered[0], t);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        playersThatAnswered.Clear();
                        return;
                    }
                    else if (playersThatAnswered[1].territoryList.Contains(t))
                    { 
                        Debug.Log($"Player {playersThatAnswered[1].playerName} " +
                                  $"already contains this territory!" +
                                  $"He defended with success from {playersThatAnswered[0].playerName}");
                        stringEvenement.Add($"Player {playersThatAnswered[1].playerName} " +
                                            $"already contains this territory!" +
                                            $"He defended with success from {playersThatAnswered[0].playerName}");
                        playersThatAnswered[1].points += 100;
                        if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                        else ServerNextTurn(playersThatAnswered[0]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        playersThatAnswered.Clear();
                        return;
                    }
                }
                else if (result_p2 > result_p1 || diff_p1 > diff_p2)
                {
                    //player 1
                    Debug.Log("Player 1 won");
                    
                    playersThatAnswered[0].hadCorrectAnswer = true;
                    playersThatAnswered[0].shouldHaveResponded = true;
                    playersThatAnswered[1].hadCorrectAnswer = false;
                    playersThatAnswered[1].shouldHaveResponded = true;


                    if (t.isFortified)
                    {
                        t.isFortified = false;
                        ServerChangeColor(t, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                        if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                        else ServerNextTurn(playersThatAnswered[1]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        //Vider la liste des joueurs
                        playersThatAnswered.Clear();
                        return;
                    }

                    
                    Debug.Log(playersThatAnswered[0].playerName + " defeated " + playersThatAnswered[1].playerName);
                    stringEvenement.Add($"{playersThatAnswered[0].playerName} defeated {playersThatAnswered[1].playerName}");

                    playersThatAnswered[0].corretQuestionCounter++;
                    
                    if (!playersThatAnswered[0].territoryList.Contains(t))
                    {
                        ServerChangeColor(t, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                        playersThatAnswered[0].territoryList.Add(t);
                        playersThatAnswered[0].points += 400;
                        playersThatAnswered[1].points -= 400;
                        ServerUpdateNeighbours(playersThatAnswered[0]);
                        ServerDeleteAndUpdate(playersThatAnswered[1], t);
                        if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                        else ServerNextTurn(playersThatAnswered[0]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }   
                        playersThatAnswered.Clear();
                        return;
                    }                        
                    
                    if (playersThatAnswered[0].territoryList.Contains(t))
                    {
                        Debug.Log($"Player {playersThatAnswered[0].playerName} " +
                                  $"already contains this territory!" +
                                  $"He defended with success from {playersThatAnswered[1].playerName}");
                        stringEvenement.Add($"Player {playersThatAnswered[0].playerName} " +
                                            $"already contains this territory!" +
                                            $"He defended with success from {playersThatAnswered[1].playerName}");
                        playersThatAnswered[0].points += 100;
                        if (playersThatAnswered[1].myTurn) ServerNextTurn(playersThatAnswered[1]);
                        else ServerNextTurn(playersThatAnswered[0]);
                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                startQnARapid = false
                            });
                        }
                        playersThatAnswered.Clear();
                        return;
                    }
                }
                
            }
            return;
        }

        /*Si le joueur a répondu a une question de rapidité, et la phase est la phase d'allocation des territoires*/
        if (msg is {type: "rapid", answered: true} && gameStatus == GamePhase.allocation_22)
        {
            //On va garder tous les joueurs qui ont bien répondu dans une liste 
            List<Player> playersWithTheRightAnswer = new List<Player>();
            
            Debug.Log($"[RAPID] [ALLOCATION] Received {msg.answer} from {msg.p.playerName} his time of response was {msg.p.timeLeft}. " +
                      $"The correct answer is {questionsRapiditeJson.questions[randomNr].correctAnswer}");
            
            msg.p.P_answer = msg.answer;
            //Comme d'habitude on va ajouter tous les joueurs qui ont répondu dans une liste
            playersThatAnswered.Add(msg.p);
            
            //Et on vérifie, si on a suffisament des joueurs
            if (playersThatAnswered.Count != players.Count)
            {
                Debug.Log("[ALLOCATION] Not everyone answered!");
            }
            else
            {
                /*Dès qu'on a suffisament des joueurs, on va commencer la validation des réponses*/
                
                foreach (var player in playersThatAnswered)
                {
                    player.questionCounter++;
                }
                
                /*On va parcourir la liste des joueurs qui ont répondu*/
                for (int i = 0; i < playersThatAnswered.Count; i++)
                {
                    /*Si un joueur a bien répondu*/
                    if (playersThatAnswered[i].P_answer == questionsRapiditeJson.questions[randomNr].correctAnswer)
                    {
                        /*On va l'ajouter dans la liste des joueurs qui ont bien répondu
                         Sinon - on va rien faire avec lui, ca veut dire qu'il n'a pas bien répondu*/
                        Debug.Log($"{playersThatAnswered[i].playerName} a bien repondu.");
                        playersWithTheRightAnswer.Add(playersThatAnswered[i]);
                        foreach (var p in playersWithTheRightAnswer)
                        {
                            p.corretQuestionCounter++;
                        }
                    }
                    else Debug.Log($"{playersThatAnswered[i].playerName} a mal repondu.");
                }
                /*Si la liste est vidé, alors il n'y a persone qui a bien répondu*/
                if (playersWithTheRightAnswer.Count == 0)
                {
                    Debug.Log("Nobody answered right");
                    /*Pour chaque joueur on va réinitialiser le timer et envoyer un message que ce QnA est fini*/
                    foreach (var player in players)
                    {
                        player.timeLeft = 20f;
                        player.connectionToClient.Send(new ServerMessage
                        {
                            startQnARapid = false
                        });
                    }

                    canExecute = false;
                    playersThatAnswered.Clear();
                    return;
                } 
                else if (playersWithTheRightAnswer.Count == 1)
                {
                    playersThatAnswered[0].corretQuestionCounter++;
                    Debug.Log($"{playersWithTheRightAnswer[0]} was the single one to respond correctly. He wins");
                    ServerChangeColor(t, playersWithTheRightAnswer[0].playerColor, playersWithTheRightAnswer[0]);
                    t.captured = true;
                    playersWithTheRightAnswer[0].territoryList.Add(t);
                    playersWithTheRightAnswer[0].points += 300;
                    terDispo--;
                    ServerUpdateNeighbours(playersWithTheRightAnswer[0]);
                    playersWithTheRightAnswer.Clear();
                    foreach (var player in players)
                    {
                        player.timeLeft = 20f;
                        player.connectionToClient.Send(new ServerMessage
                        {
                            startQnARapid = false
                        });
                    }
                    playersThatAnswered.Clear();
                    canExecute = false;
                    return;
                } 
                else if (playersWithTheRightAnswer.Count > 1)
                {
                    Player winner = null;

                    // Loop over the list of players with the right answer
                    foreach (var player in playersWithTheRightAnswer)
                    {
                        // Check if we haven't set a winner yet, or if this player has a higher timeLeft value than the current winner
                        if (winner == null || player.timeLeft > winner.timeLeft)
                        {
                            // Set the winner to this player
                            winner = player;
                        }
                    }
                    winner.corretQuestionCounter++;
                    Debug.Log($"{winner.playerName} has the smallest response time, he wins!");
                    winner.territoryList.Add(t);
                    winner.points += 300;
                    ServerChangeColor(t, winner.playerColor, winner);
                    t.captured = true;
                    ServerUpdateNeighbours(winner);
                    playersWithTheRightAnswer.Clear();
                    terDispo--;
                    playersThatAnswered.Clear();
                    foreach (var player in players)
                    {
                        player.timeLeft = 20f;
                        player.connectionToClient.Send(new ServerMessage
                        {
                            startQnARapid = false
                        });
                    }

                    canExecute = false;
                    return;
                }
            }
        }
        
        if (msg.answered && gameStatus == GamePhase.expantion_21)
        {
            msg.p.P_answer = msg.answer;
            playersThatAnswered.Add(msg.p);

            foreach (var player in playersThatAnswered)
            {
                player.questionCounter++;
            }
            
            if (playersThatAnswered.Count != players.Count)
            {
                Debug.Log("Not everyone answered!");
            }
            else
            {
                for (int i = 0; i < playersThatAnswered.Count; i++)
                {
                    if (playersThatAnswered[i].P_answer == questionsJson.questions[randomNr].correctAnswer)
                    {
                        playersThatAnswered[i].corretQuestionCounter++;
                        Debug.Log(playersThatAnswered[i].playerName + " a bien repondu, il va garder son territoire");
                        stringEvenement.Add(playersThatAnswered[i].playerName + " a bien repondu, il va garder son territoire");
                        Debug.Log($"BEFORE SELECTEDTERRITORY CAPTURED {playersThatAnswered.Count} {playersThatAnswered[i].selectedTerritoryExpansion.Count}");
                        playersThatAnswered[i].territoryList.Add(playersThatAnswered[i].selectedTerritoryExpansion[0]);
                        playersThatAnswered[i].points += 200;
                        //update neighbours list. We need to look through the neighbours of each territory a player owns,
                        //and add it to the player neighbours list if it is not already there

                        ServerUpdateNeighbours(playersThatAnswered[i]);
                        terDispo--;
                    }
                    else
                    {
                        Debug.Log(playersThatAnswered[i].playerName + " a mal repondu, il va pas garder son territoire");
                        stringEvenement.Add(playersThatAnswered[i].playerName + " a mal repondu, il va pas garder son territoire");
                        Debug.Log($"BEFOR SELECTEDTERRITORY CAPTURED {playersThatAnswered.Count} {playersThatAnswered[i].selectedTerritoryExpansion.Count}");
                        playersThatAnswered[i].selectedTerritoryExpansion[0].captured = false;
                        Debug.Log($"CAPTURED OK {playersThatAnswered.Count} {playersThatAnswered[i].selectedTerritoryExpansion.Count}");
                        playersThatAnswered[i].selectedTerritoryExpansion[0].territoryColor = Color.white;
                        Debug.Log($"CHANGED COLOR OK {playersThatAnswered.Count} {playersThatAnswered[i].selectedTerritoryExpansion.Count}");
                    }
                }
                
                roundsPhase2++;
                
                Debug.Log("Territoires disponibles : " + terDispo);
                Debug.Log("Current round for phase 2 : " + roundsPhase2);

                foreach (var player in players)
                {
                    player.connectionToClient.Send(new ServerMessage
                    {
                        startQnA = false
                    });
                }

                foreach (var p in players)
                {
                    p.selectedTerritoryExpansion.Clear();
                }

                Debug.Log("[TERRITORY SELECTION PHASE] Everything cleared");
            
                playersThatAnswered.Clear();
                return;
            }
        }
        
        if (msg is {type: "simple", answered: true} && gameStatus == GamePhase.battle_3)
        {
            msg.p.P_answer = msg.answer;
            
            playersThatAnswered.Add(msg.p);
            
            foreach (var player in playersThatAnswered)
            {
                player.questionCounter++;
            }
            
            foreach (var player in players)
            {
                player.shouldHaveResponded = false;
            }
            
            Debug.Log(playersThatAnswered.Count);
            Debug.Log("Adding : " + msg.p.playerName + " to the list of players that answered");

            if (playersThatAnswered.Count != 2)
            {
                Debug.Log("Not everybody answered!");
                Debug.Log("Current try of " + msg.p.playerName + " for territory : " + msg.p.currentTryTerritory);
            }
            else
            {
                if ((playersThatAnswered[0].P_answer == questionsJson.questions[randomNr].correctAnswer
                     && playersThatAnswered[1].P_answer == questionsJson.questions[randomNr].correctAnswer))
                {
                    randomNr = GenerateRandomRapid();
                    
                    Debug.Log("Both players answered right. Start question de rapidite");
                    
                    playersThatAnswered[0].corretQuestionCounter++;
                    playersThatAnswered[1].corretQuestionCounter++;
                    

                    t = msg.P_currentTerritory;
                    
                    foreach (var player in players)
                    {
                        player.connectionToClient.Send(new ServerMessage
                        {
                            currentTer = t,
                            qR = questionsRapiditeJson.questions[randomNr],
                            startQnARapid = true,
                            draw = true,
                            phase = gameStatus
                        });
                    }

                    playersThatAnswered.Clear();
                    return;
                }

                if ((playersThatAnswered[0].P_answer != questionsJson.questions[randomNr].correctAnswer 
                     && playersThatAnswered[1].P_answer != questionsJson.questions[randomNr].correctAnswer))
                {
                    if (playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                    else ServerNextTurn(playersThatAnswered[1]);
                    
                    Debug.Log("Both players answered wrong. Do nothing");

                    playersThatAnswered[0].hadCorrectAnswer = false;
                    playersThatAnswered[0].shouldHaveResponded = true;
                    playersThatAnswered[1].hadCorrectAnswer = false;
                    playersThatAnswered[1].shouldHaveResponded = true;


                    foreach (var player in players)
                    {
                        player.connectionToClient.Send(new ServerMessage
                        {
                            startQnA = false
                        });
                    }
                    playersThatAnswered.Clear();
                    return;
                }
                else
                {
                    if (playersThatAnswered[0].isAtacking)
                    {
                        Debug.Log(playersThatAnswered[0] + " is attacking " + playersThatAnswered[1]);
                        if ((playersThatAnswered[0].P_answer == questionsJson.questions[randomNr].correctAnswer)
                            && (playersThatAnswered[1].P_answer != questionsJson.questions[randomNr].correctAnswer))
                        {
                            if (msg.P_currentTerritory.isFortified)
                            {
                                msg.P_currentTerritory.isFortified = false;
                                ServerChangeColor(msg.P_currentTerritory, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                                if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                                else ServerNextTurn(playersThatAnswered[1]);
                                foreach (var player in players)
                                {
                                    player.connectionToClient.Send(new ServerMessage
                                    {
                                        startQnARapid = false
                                    });
                                }
                                //Vider la liste des joueurs
                                playersThatAnswered.Clear();
                                return;
                            }
                            
                            playersThatAnswered[0].corretQuestionCounter++;
                            Debug.Log(playersThatAnswered[0].playerName + " defeated " + playersThatAnswered[1].playerName);
                            stringEvenement.Add(playersThatAnswered[0].playerName + " defeated " + playersThatAnswered[1].playerName);
                            playersThatAnswered[0].hadCorrectAnswer = true;
                            playersThatAnswered[0].shouldHaveResponded = true;
                            playersThatAnswered[1].hadCorrectAnswer = false;
                            playersThatAnswered[1].shouldHaveResponded = true;

                            Debug.Log("[=====INFO=====]Capturing his territory");
                            ServerChangeColor(msg.P_currentTerritory, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                            playersThatAnswered[0].points += 400;
                            playersThatAnswered[1].points -= 400;
                            playersThatAnswered[0].territoryList.Add(msg.P_currentTerritory);

                            //update neighbours based on players territory list
                            //foreach ter in player.territoryList
                            //remove all neighbours, and add the neighbours of the players territory list

                            ServerUpdateNeighbours(playersThatAnswered[0]);

                            ServerDeleteAndUpdate(playersThatAnswered[1], msg.P_currentTerritory);

                            // Add the lost territory back as a neighbor
                            playersThatAnswered[1].neighbours.Add(msg.P_currentTerritory);

                            playersThatAnswered[0].P_answer = null;
                            if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnA = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                        if ((playersThatAnswered[1].P_answer == questionsJson.questions[randomNr].correctAnswer)
                                 && (playersThatAnswered[0].P_answer !=
                                     questionsJson.questions[randomNr].correctAnswer))
                        {
                            playersThatAnswered[1].corretQuestionCounter++;
                            playersThatAnswered[1].hadCorrectAnswer = true;
                            playersThatAnswered[1].shouldHaveResponded = true;
                            playersThatAnswered[0].hadCorrectAnswer = false;
                            playersThatAnswered[0].shouldHaveResponded = true;
                            Debug.Log(playersThatAnswered[1].playerName + " defended with success from " + playersThatAnswered[0].playerName);
                            stringEvenement.Add(playersThatAnswered[1].playerName + " defended with success from " + playersThatAnswered[0].playerName);
                            //ajouter 100 points a playersThatAnswered[1]
                            playersThatAnswered[1].points += 100;
                            
                            if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnA = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                    }
                    
                    if (playersThatAnswered[1].isAtacking)
                    {
                        Debug.Log(playersThatAnswered[1] + " is attacking " + playersThatAnswered[0]);
                        if ((playersThatAnswered[1].P_answer == questionsJson.questions[randomNr].correctAnswer)
                            && (playersThatAnswered[0].P_answer != questionsJson.questions[randomNr].correctAnswer))
                        {
                            if (msg.P_currentTerritory.isFortified)
                            {
                                msg.P_currentTerritory.isFortified = false;
                                ServerChangeColor(msg.P_currentTerritory, playersThatAnswered[0].playerColor, playersThatAnswered[0]);
                                if(playersThatAnswered[0].myTurn) ServerNextTurn(playersThatAnswered[0]);
                                else ServerNextTurn(playersThatAnswered[1]);
                                foreach (var player in players)
                                {
                                    player.connectionToClient.Send(new ServerMessage
                                    {
                                        startQnARapid = false
                                    });
                                }
                                //Vider la liste des joueurs
                                playersThatAnswered.Clear();
                                return;
                            }
                            
                            playersThatAnswered[1].corretQuestionCounter++;
                            Debug.Log(playersThatAnswered[1].playerName + " defeated " + playersThatAnswered[0].playerName);
                            stringEvenement.Add(playersThatAnswered[1].playerName + " defeated " + playersThatAnswered[0].playerName);
                            playersThatAnswered[1].hadCorrectAnswer = true;
                            playersThatAnswered[1].shouldHaveResponded = true;
                            playersThatAnswered[0].hadCorrectAnswer = false;
                            playersThatAnswered[0].shouldHaveResponded = true;
                            Debug.Log("[=====INFO=====]Capturing his territory");
                            ServerChangeColor(msg.P_currentTerritory, playersThatAnswered[1].playerColor, playersThatAnswered[1]);
                            playersThatAnswered[1].points += 400;
                            playersThatAnswered[0].points -= 400;
                            playersThatAnswered[1].territoryList.Add(msg.P_currentTerritory);
                            
                            ServerUpdateNeighbours(playersThatAnswered[1]);
                            
                            ServerDeleteAndUpdate(playersThatAnswered[0], msg.P_currentTerritory);
                            
                            playersThatAnswered[1].P_answer = null;
                            if (playersThatAnswered[1].myTurn)
                            {
                                ServerNextTurn(playersThatAnswered[1]);
                            }
                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnA = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                        if ((playersThatAnswered[0].P_answer == questionsJson.questions[randomNr].correctAnswer)
                            && (playersThatAnswered[1].P_answer !=
                                questionsJson.questions[randomNr].correctAnswer))
                        {
                            playersThatAnswered[0].corretQuestionCounter++;
                            Debug.Log(playersThatAnswered[0].playerName + " defended with success from " + playersThatAnswered[1].playerName);
                            stringEvenement.Add(playersThatAnswered[0].playerName + " defended with success from " + playersThatAnswered[1].playerName);
                            playersThatAnswered[0].hadCorrectAnswer = true;
                            playersThatAnswered[0].shouldHaveResponded = true;
                            playersThatAnswered[1].hadCorrectAnswer = false;
                            playersThatAnswered[1].shouldHaveResponded = true;

                            //ajouter 100 points au playersThatAnswered[0]
                            playersThatAnswered[0].points += 100;
                            
                            if (playersThatAnswered[1].myTurn)
                            {
                                ServerNextTurn(playersThatAnswered[1]); 
                            }

                            foreach (var player in players)
                            {
                                player.connectionToClient.Send(new ServerMessage
                                {
                                    startQnA = false
                                });
                            }
                            playersThatAnswered.Clear();
                            return;
                        }
                    }
                }
            }
        }
        
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  La fonction OnReceive va gerer tous les messages provenant d'un territoire. Il va recevoir une
                    structure TerritoryMessage qui contient toutes les information qu'il a besoin. 
                    Regarder "Territory.cs" pour voir la structure ClientMessage                                      */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void OnReceiveTerritory(NetworkConnectionToClient conn, TerritoryMessage msg)
    {
        if (msg.territoryClicked && msg.QnA && gameStatus == GamePhase.battle_3)
        {
            randomNr = GenerateRandom();
            //Envoyer de question a tous les joueurs
            
            foreach (var player in players)
            {
                player.connectionToClient.Send(new ServerMessage
                {
                    currentTer = msg.t,
                    startQnA = true,
                    desactiverTer = true,
                    q = questionsJson.questions[randomNr],
                    phase = gameStatus
                });
            }
            return;

        }
        else
        {
            ServerChangeColor(msg.t, msg.p.playerColor, msg.p);
            ServerNextTurn(msg.p);
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] : Les deux fonctions pour generer un index random pour les listes des questions                      */
    /*----------------------------------------------------------------------------------------------------------------*/
    public int GenerateRandom() { return Random.Range(0, questionsJson.questions.Length-1); }
    
    public int GenerateRandomRapid() { return Random.Range(0, questionsRapiditeJson.questions.Length-1); }

    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Cette methode va ajouter un joueur dans la liste de joueurs qu'il doit gerer.
                    Le serveur va garder cette liste pendant un match.                                                */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerAddPlayer(Player player)
    {
        Debug.Log("[SERVER] Adding player "+ player.playerName+ " to the list");
        players.Add(player);
        playersThatPlay.Add(player);
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le serveur va renvoyer une couleur random qu'il va prendre dans la liste _colorList.              */
    /*----------------------------------------------------------------------------------------------------------------*/
    public Color ServerAssignColor()
    {
        var rand = Random.Range(0, _colorList.Count);
        var tempColor = _colorList[rand];
        _colorList.Remove(_colorList[rand]);
        return tempColor;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Changer la couleur d'un territoire basé sur son état.                                             */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerChangeColor(Territory t, Color c, Player p)
    {
        Debug.Log("[SERVER] Player " + p.name + " changed the color of territory " + t);
        float baseBrightness = 0.3f;
        float fortifiedBrightness = 0.6f;
    
        if (t.isBase)
        {
            t.territoryColor = p.playerColor;
            Color.RGBToHSV(t.territoryColor, out float hue, out float saturation, out float brightness);
            t.territoryColor = Color.HSVToRGB(hue, saturation, baseBrightness);
        }
        else if (t.isFortified)
        {
            t.territoryColor = p.playerColor;
            Color.RGBToHSV(t.territoryColor, out float hue, out float saturation, out float brightness);
            t.territoryColor = Color.HSVToRGB(hue, saturation, fortifiedBrightness);
        }
        else
        {
            t.territoryColor = p.playerColor;
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le serveur va donner le premier tour a un joueur random dans la liste players.                    */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerSetFirstTurn()
    {
        //Si il y a pas assez des joueurs, le jeu va pas commencer (normalement)
        if (players.Count != 3)
        {
            Debug.Log("[SERVER] Waiting for players.");
        }
        else
        {
            //int rand = Random.Range(0, players.Count);
            players[0].myTurn = true;
        }
    }

    IEnumerator Waiter()
    {
        yield return new WaitForSeconds(5f);
    }
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Gerer le tours des joueurs.                                                                       */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerNextTurn(Player p)
    {
        /* Si on a moins de 2 joueurs, on sors de la fonction */
        if (playersThatPlay.Count < 3 && gameStatus == GamePhase.baseSelect_1)
        {
            Debug.Log("[INFO] There are not enough players!");
            return;
        }

        /* Sinon, on cherche l'index du joueur en question */
        int currIndex = playersThatPlay.IndexOf(p);
        
        
        /* Si l'index est le dernier index du liste, normalement le player suivant c'est le premier dans la liste */
        if (currIndex == playersThatPlay.Count - 1)
        {
            //Si il a pas de ter, supprimer le
            if (playersThatPlay[currIndex].territoryList.Count == 0 && gameStatus == GamePhase.battle_3)
            {
                playersThatPlay[currIndex].myTurn = false;
                playersThatPlay[0].myTurn = true;
                return;
            }
            
            Debug.Log("[INFO] It is the turn of player : " + playersThatPlay[0].playerName);
            Debug.Log($"Players that play count : {playersThatPlay.Count}. Current index : {currIndex}");
            playersThatPlay[currIndex].myTurn = false;

            Debug.Log("CHANGED TURN OK");
            
            StartCoroutine(Waiter());
            
            if(gameStatus == GamePhase.expantion_21 || gameStatus == GamePhase.battle_3)ServerUnblock();
            if (gameStatus == GamePhase.expantion_21)
            {
                randomNr = GenerateRandom();

                Debug.Log($"Questions count : {questionsJson.questions.Length}");
                Debug.Log($"Randomly generated number: {randomNr}");

                foreach (var player in players)
                {
                    player.connectionToClient.Send(new ServerMessage
                    {
                        startQnA = true,
                        desactiverTer = true,
                        q = questionsJson.questions[randomNr],
                        phase = gameStatus
                    });
                }
            }
            if(gameStatus == GamePhase.expantion_21 || gameStatus == GamePhase.battle_3)ServerBlockInteraction(playersThatPlay[0]);
            if (gameStatus == GamePhase.battle_3) roundsPhase3++;
            playersThatPlay[0].myTurn = true;
        }

        /*Si l'index est pas le dernier dans la liste,
          c'est le tour au joueur au index currIndex, on passe le tour au joueur voisin (currIndex+1)*/
        else if (playersThatPlay[currIndex].myTurn)
        {
            if (playersThatPlay[currIndex].territoryList.Count == 0 && gameStatus == GamePhase.battle_3)
            {
                playersThatPlay[currIndex].myTurn = false;
                playersThatPlay[currIndex + 1].myTurn = true;
                return;
            }
            
            Debug.Log("[INFO] Player " + playersThatPlay[currIndex] + " finished his turn");
            Debug.Log("[INFO] It is the turn of player : " + playersThatPlay[currIndex + 1].playerName);
            playersThatPlay[currIndex].myTurn = false;

            if (terDispo < players.Count && gameStatus == GamePhase.expantion_21)
            {
                //Lancer question de rapidite pour tout le monde
                playersThatPlay[currIndex].selectedTerritoryExpansion.Clear();
            }
            
            if(gameStatus == GamePhase.expantion_21 || gameStatus == GamePhase.battle_3)ServerUnblock();
            if(gameStatus == GamePhase.expantion_21 || gameStatus == GamePhase.battle_3)ServerBlockInteraction(playersThatPlay[currIndex+1]);
            playersThatPlay[currIndex + 1].myTurn = true;
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Montrer a un joueur ou il peut cliquer.                                                           */
    /*----------------------------------------------------------------------------------------------------------------*/
    void ServerBlockInteraction(Player p)
    {
        foreach (var ter in territoriesList)
        {
            if (gameStatus == GamePhase.expantion_21)
            {
                if (!p.neighbours.Contains(ter) || ter.captured || ter.isBase)
                {
                    ter.territoryColor = new Color(ter.territoryColor.r, ter.territoryColor.g, ter.territoryColor.b, 0.3f);
                }   
            }
            else if (gameStatus == GamePhase.battle_3)
            {
                if (!p.neighbours.Contains(ter))
                {
                    ter.territoryColor = new Color(ter.territoryColor.r, ter.territoryColor.g, ter.territoryColor.b, 0.3f);
                }
            }
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Remettre toutes les territoires en couleur originale                                              */
    /*----------------------------------------------------------------------------------------------------------------*/
    void ServerUnblock()
    {
        foreach (var ter in territoriesList)
        {
            ter.territoryColor = new Color(ter.territoryColor.r, ter.territoryColor.g, ter.territoryColor.b, 1f);
        }
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Trouver le joueur gagnant                                                                         */
    /*----------------------------------------------------------------------------------------------------------------*/
    Player GetWinnerByPoints()
    {
        Player winner = null;
        foreach (Player player in players)
        {
            // Check if we haven't set a winner yet, or if this player has a higher points value than the current winner
            if (winner == null || player.points > winner.points)
            {
                // Set the winner to this player
                winner = player;
            }
        }
        return winner;
    }
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Cette fonction va mettre a jour la liste des voisins d'un joueur.
                    On l'appelle lors  d'une capturation d'un territoire                                              */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerUpdateNeighbours(Player p)
    {
     
        // Iterate over all territories owned by the player
        foreach (var territory in p.territoryList)
        {
            // Iterate over each neighbour of the current territory
            foreach (var neighbour in territory.neighbours)
            {
                // Check if the neighbour is not already owned by the player and is directly adjacent to one of their territories
                if (!p.territoryList.Contains(neighbour) && territory.neighbours.Contains(neighbour))
                {
                    // Check if the neighbour is not already in the player's neighbours list
                    if (!p.neighbours.Contains(neighbour))
                    {
                        p.neighbours.Add(neighbour); // Add the neighbour to the player's neighbours list
                    }
                }
                else
                {
                    // Check if the neighbour is already owned by the player and is in their neighbours list
                    if (p.territoryList.Contains(neighbour) && p.neighbours.Contains(neighbour))
                    {
                        p.neighbours.Remove(neighbour); // Remove the neighbour from the player's neighbours list
                    }
                }
            }
        }   
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Cette fonction va mettre a jour la liste des voisins d'un joueur qui a perdu son territoire
                    pendant la phase de bataille. On l'appelle lors  d'une capturation d'un territoire.               */
    /*----------------------------------------------------------------------------------------------------------------*/
    public void ServerDeleteAndUpdate(Player p, Territory t)
    {
        p.territoryList.Remove(t);
                            
        // Update the player's list of neighbors based on the remaining territories, including the lost territory
        // Clear the player's neighbours list
        p.neighbours.Clear();

        // Update the player's list of neighbors based on the remaining territories
        foreach (var territory in p.territoryList)
        {
            foreach (var neighbour in territory.neighbours)
            {
                if (!p.territoryList.Contains(neighbour) 
                    && !p.neighbours.Contains(neighbour)
                    && neighbour != t)
                {
                    p.neighbours.Add(neighbour);
                }
            }
        }                 
                            
        // Add the lost territory back as a neighbor
        p.neighbours.Add(t);
    }

    /*Des booleans pour rentrer une seule fois dans une condition if dans la fonction Update*/
    private bool canExecute = false;
    private bool canExec = false;
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Cette fonction va envoyer toutes les données d'une partie a la BDD en utilisant la classe
                    ServerSideScripts qui contient toutes les fonctions pour envoyer des messages a la BDD.
                    On envoie les scores, les evenements, le gagnant, et les joueurs d'une partie.                    */
    /*----------------------------------------------------------------------------------------------------------------*/
    public IEnumerator RegisterEndMatch(Player p)
    {
        yield return StartCoroutine(_serverSideScripts.SendEvenementToServer(stringEvenement.ToArray(), gameID));

        yield return StartCoroutine(_serverSideScripts.SendWinner(p, gameID));

        yield return StartCoroutine(_serverSideScripts.SendPlayed(gameID));

        yield return StartCoroutine(_serverSideScripts.FinDeLaPartie(gameID));

        for (int i = 0; i < players.Count; i++)
        {
            yield return StartCoroutine(_serverSideScripts.SendPoints(players[i]));
        }
    }
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Ici se passe la gestion des phases pendant une partie. Il existe plusieurs cas dans lesquels le
                    GameManager peut rentrer. Pour chaque cas il a des actions specifiques a faire                    */
    /*----------------------------------------------------------------------------------------------------------------*/
    private void Update()
    {
        foreach (var player in playersThatPlay)
        {
            if (player.territoryList.Count == 0 && gameStatus == GamePhase.battle_3)
            {
                Debug.Log(player.playerName + " has no territories. Deleting him from the players that play!");
                playersThatPlay.Remove(player);
            }
        }

        
        if (phase1Fin == false)
        {
            if (playersWithBase.Count < players.Count)
            {
                //Debug.Log("Not everyone has a base");
            }
            else
            {
                Debug.Log("Everyone has a base! Phase 1 ending! Phase 2 beginning");
                gameStatus = GamePhase.expantion_21;
                foreach (var p in players)
                {
                    p.phase = gameStatus;
                }
                phase1Fin = true;
            }
        }

        if (phase2Fin == false && phase1Fin == true)
        {
            if (terDispo == 0 || (roundsPhase2 == 6 && terDispo == 0))
            {
                Debug.Log("Phase 2 ending! Phase 3 beginning!");
                foreach (Player p in playersThatAnswered)
                {
                    p.currentTryTerritory = null;
                    p.P_answer = null;
                    p.canAnswer = false;
                    p.isAtacking = false;
                    p.p_answered = false;
                }
                gameStatus = GamePhase.battle_3;
                foreach (var p in players)
                {
                    p.phase = gameStatus;
                }
                playersThatAnswered.Clear();
                phase2Fin = true;
                return;
            }
            
            //Si nbTours = 6 et il reste encore des territoires, on va passer a la phase d'allocation
            if (
                ((roundsPhase2 == 6 && (terDispo != 0 || terDispo < players.Count)) || (roundsPhase2 < 6 && terDispo < players.Count))
                && canExecute == false)
            {
                Debug.Log("Territoires disponibles : " + terDispo);
                Debug.Log("Current round for phase 2 : " + roundsPhase2);

                Debug.Log("[INFORMATION PHASE ALLOCATION] PHASE ALLOCATION STARTED FIRST IF");
                gameStatus = GamePhase.allocation_22;
                //lancer question rapidite
                foreach (var ter in GameObject.FindGameObjectsWithTag("Territory"))
                {
                    if (!ter.GetComponent<Territory>().captured)
                    {
                        t = ter.GetComponent<Territory>();
                     
                        Debug.Log($"{t.name} is not captured. Sending it to the players");
                        
                        randomNr = GenerateRandomRapid();

                        foreach (var player in players)
                        {
                            player.connectionToClient.Send(new ServerMessage
                            {
                                currentTer = ter.GetComponent<Territory>(),
                                qR = questionsRapiditeJson.questions[randomNr],
                                startQnARapid = true,
                                draw = true,
                                phase = gameStatus
                            });
                        }
                        
                        canExecute = true;
                        return;
                    }
                }
            }
        }

        if (phase3Fin == false && phase2Fin == true && phase1Fin == true)
        {
            foreach (var player in players)
            {
                if (player.territoryList.Count == territoriesList.Count || roundsPhase3 == 6)
                {
                    Debug.Log("Phase 3 ending! Match ending!");
                    
                    Player win = GetWinnerByPoints();
                    
                    Debug.Log($"{win.playerName} won with {win.points}");
                    
                    if (canExec == false)
                    {
                        StartCoroutine(RegisterEndMatch(win));
                        for (int i = 0; i < players.Count; i++)
                        {
                            StartCoroutine(_serverSideScripts.SendPoints(players[i]));
                            StartCoroutine(_serverSideScripts.SendNumberOfQuestions(gameID, players[i]));
                            StartCoroutine(_serverSideScripts.SendNumberOfCorrectQuestions(gameID, players[i]));
                        }
                        canExec = true;
                    }
                    
                    foreach (var terDel in territoriesList)
                    {
                        terDel.GetComponent<Territory>().player = null;
                        terDel.GetComponent<Territory>().currentPhase = 0;
                        terDel.GetComponent<Territory>().territoryColor = Color.white;
                        terDel.GetComponent<Territory>().isBase = false;
                        terDel.GetComponent<Territory>().isFortified = false;
                        terDel.GetComponent<Territory>().captured = false;
                        
                        NetworkServer.Destroy(terDel.gameObject);
                    }

                    playersThatPlay.Clear();
                    playersThatAnswered.Clear();
                    playersWithBase.Clear();
                    territoriesList.Clear();
                    Debug.Log("[SERVER] Everything cleared. Match ended with success!");

                    for (int i = 0; i < players.Count; i++)
                    {
                        if (players[i] == win)
                        {
                            win.connectionToClient.Send(new ServerMessage
                            {
                                fin = true,
                                winner = true
                            });       
                        }
                        else
                        {
                            players[i].connectionToClient.Send(new ServerMessage
                            {
                                fin = true,
                                winner = false
                            });
                        }
                    }
                    
                    players.Clear();
                    phase3Fin = true;
                }
            }
        }

        if (_notExecuted)
        {
            if (GameObject.FindGameObjectsWithTag("Territory").Length != 0)
            {
                foreach (var territory in GameObject.FindGameObjectsWithTag("Territory"))
                {
                    Debug.Log("Adding " + territory.name + " to the list");
                    if (!territoriesList.Contains(territory.GetComponent<Territory>()))
                    {
                        territoriesList.Add(territory.GetComponent<Territory>());
                    }
                }

                terDispo = territoriesList.Count;
                Debug.Log("[SERVER] All territories added to the list");
            }
            else
            {
                Debug.Log("[SERVER] No territories to add");
            }
            _notExecuted = false;
        }
    }
    
    /*Région responsable des questions. Systeme fait par Mohammed Ali */
    #region QnA

    //une liste de type QuestionsAndAnsware
    public List<CLQuestion> QnA = new();
    public List<CLQuestionRapid> QnARapid = new();
   
    
    [Serializable]
    public class Questions
    {
        public CLQuestion[] questions;
    }
    
    [Serializable]
    public class QuestionsRapidite
    {
        public CLQuestionRapid[] questions;
    }

    //initialiser les field des questions et des reponses 100 c juste pour une petite version apres ca depends de questions de la bdd
    public void initField(){
        for (int i = 0; i < 121; i++)
        {
            QnA.Add(new());
            QnA[i].Question = "";
            QnA[i].Answers = new string[4] ;
        }
    }

    public void initFieldRapid()
    {
        for (int i = 0; i < 124; i++)
        {
            QnARapid.Add(new());
            QnARapid[i].Question = "";
            QnARapid[i].correctAnswer = "";
        }
    }
    

    private IEnumerator GetJSON()
    {
        string url = "192.168.100.104/question_multiple.php";
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            Debug.Log("[SERVER -> SUCCESS] unitywebrequest.get succeeded at 192.168.100.104/question_multiple.php");

            yield return www.SendWebRequest();
            Debug.Log("juste apres www.SendWebRequest()");

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                initField();
                json = www.downloadHandler.text;
                questionsJson = JsonUtility.FromJson<Questions>(json);
            }
        }
    }
    
    private IEnumerator GetJSONRapid()
    {
        string url = "192.168.100.104/question_simple.php";
        using (UnityWebRequest www = UnityWebRequest.Get(url))
        {
            Debug.Log("[SERVER -> SUCCESS] unitywebrequest.get succeeded at 192.168.100.104/question_simple.php");

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                initFieldRapid();
                jsonRapid = www.downloadHandler.text;
                questionsRapiditeJson = JsonUtility.FromJson<QuestionsRapidite>(jsonRapid);
            }
        }
    }
        
        
    
    #endregion
}
