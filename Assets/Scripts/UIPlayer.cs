using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPlayer : MonoBehaviour
{
    [SerializeField] TMP_Text text;

    public void SetPlayer(Player player) {
        text.text = player.playerName;
    }
}
