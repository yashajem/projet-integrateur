using System;

[Serializable]
public class EvenementPartie
{
    public string[] evenements;
    public int partie;
    
    public EvenementPartie(string[] p, int n) {
        evenements = p;
        partie = n;
    }
}