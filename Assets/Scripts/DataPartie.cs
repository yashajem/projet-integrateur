using System;

[Serializable]
public class DataPartie
{
    public int[] participants;
    public int partie;
    
    public DataPartie(int[] p, int n) {
        participants = p;
        partie = n;
    }
}
