using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;

public class GameUIPlayer : MonoBehaviour
{

    public TMP_Text username;
    public TMP_Text points;
    public TMP_Text turn;
    public Player player;

    public void SetPlayer(Player p)
    {
        username.text = p.playerName;
        username.color = p.playerColor;
        points.text = p.points.ToString();
        if (p.myTurn)
        {
            turn.text = "C'est ton tour!";
            turn.color = Color.green;
        }
        else
        {
            turn.text = "Ce n'est pas ton tour!";
            turn.color = Color.red;
        }
        player = p;
    }
}
