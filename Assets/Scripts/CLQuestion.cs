using System;

[Serializable]
public class CLQuestion
{
    public int ID;
    public string Question;
    public string[] Answers;
    public string correctAnswer;
}
