using System;
using System.Collections;
using Mirror;
using UnityEngine;
using UnityEngine.Networking;

public class Connection : MonoBehaviour
{
    [SerializeField]
    private NetworkManager manager;

    public Camera cam;
    public AudioSource audio;
    public AudioSource click;
    public GameObject creationCompteUI;
    public GameObject loginUI;
    
    public TriviareignAuthenticator b;

    public static Connection instance;
    
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] cams = GameObject.FindGameObjectsWithTag("MainCamera");
        GameObject[] objs = GameObject.FindGameObjectsWithTag("AudioPrincipale");
        GameObject[] clicks = GameObject.FindGameObjectsWithTag("SonClick"); 

        if (cams.Length > 1)
        {
            Destroy(cams[1]);
        }

        if (objs.Length > 1)
        {
            Destroy(objs[1]);
        }

        if (clicks.Length > 1)
        {
            Destroy(clicks[1]);
        }

        DontDestroyOnLoad(click.gameObject);
        DontDestroyOnLoad(cam.gameObject);
        DontDestroyOnLoad(audio.gameObject);
        
        b = GameObject.FindWithTag("Network").GetComponent<TriviareignAuthenticator>();
        if (!Application.isBatchMode)
        {
            manager.autoStartServerBuild = false;
            Debug.Log("Client build");
        }
        else
        {
            manager.autoStartServerBuild = true;
            Console.Clear();
            Debug.Log("[INFO -> SUCCESS] | Server build");
        }
    }

    public void CreationCompteAction()
    {
        loginUI.SetActive(true);
        creationCompteUI.SetActive(false);
    }
    
    public void LoginAction()
    {
        b.login = true;
        b.reg = false;
        b.forgot = false;
        Debug.Log("Connecting to server : " + manager.networkAddress);
        manager.StartClient();
    }

    public void RegisterAction()
    {
        b.login = false;
        b.reg = true;
        b.forgot = false;
        Debug.Log("Connecting to server : " + manager.networkAddress);
        manager.StartClient(); 
    }

    public void ForgotPasswordAction()
    {
        b.login = false;
        b.reg = false;
        b.forgot = true;
        manager.StartClient();
    }

    public void ResetPasswordAction()
    {
        b.login = false;
        b.reg = false;
        b.forgot = false;
        b.resetPass = true;
        manager.StartClient();
    }
}

