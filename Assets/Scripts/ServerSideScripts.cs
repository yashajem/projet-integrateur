using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

/* */

public class ServerSideScripts
{
    //A la fin du jeu send les points (Pour l'instant on va check avec un simple click)
    public IEnumerator SendPoints(Player player)
    {
        WWWForm form = new WWWForm();
        form.AddField("login", player.playerName);
        form.AddField("score", player.points);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/updateScore.php", form))
        {
            //playername does not register
            Debug.Log("Sending : " + player.playerName + " data to database. Data sent : " + player.points);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);
                
                if (response == 200)
                {
                    Debug.Log("[SERVER -> INFO] Everything went well. Classement updated");
                }
                
                if (response == 401)
                {
                    Debug.Log("[SERVER -> ERROR] The user does not exist in the database");
                }

                if (response == 404)
                {
                    Debug.Log("[SERVER -> ERROR] Champ vide, le serveur a pas bien recu les donnees");
                }

                if (response == 402)
                {
                    Debug.Log("[SERVER- > ERROR] Update a echoué ");
                }

                if (response == 403)
                {
                    Debug.Log("[SERVER -> ERROR] Update de classement a echoué");
                }
            }
        }
    }

    public IEnumerator SendNumberOfCorrectQuestions(int gameID, Player p)
    {
        WWWForm form = new WWWForm();
        form.AddField("partie", gameID);
        form.AddField("participant", p.ID);
        form.AddField("questionsCounter", p.corretQuestionCounter);
        
        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/ajout_questions_corrects.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);
                Debug.Log("Response envoi questions correctes: " + response);
            }
        }
    }
    
    public IEnumerator SendNumberOfQuestions(int gameID, Player p)
    {
        WWWForm form = new WWWForm();
        form.AddField("partie", gameID);
        form.AddField("participant", p.ID);
        form.AddField("questionsCounter", p.questionCounter);
        
        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/ajout_total_questions.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                Debug.Log("Response envoi questions totales: " + response);
            }
        }
    }

    
    public IEnumerator SendPlayed(int gameID)
    {
        WWWForm form = new WWWForm();
        form.AddField("partie", gameID);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/ajout_partie_jouee.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 400)
                {
                    Debug.Log("[SERVER -> ERROR] Update error");
                }

                if (response == 401)
                {
                    Debug.Log("[SERVER -> ERROR] Champs vides");
                }

                if (response == 402)
                {
                    Debug.Log("[SERVER -> ERROR] Partie non-existante");
                }

                if (response == 200)
                {
                    Debug.Log("[SERVER -> SUCCESS] Tout est bien");
                }
            }
        }
    }
    
    public IEnumerator SendWinner(Player p, int gameID)
    {
        WWWForm form = new WWWForm();
        form.AddField("partie", gameID);
        form.AddField("participant", p.ID);

        Debug.Log($"Sent player {p.playerName} with ID : {p.ID}");

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/ajout_partie_gagnee.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 400)
                {
                    Debug.Log("[SERVER -> ERROR] Update error");
                }

                if (response == 401)
                {
                    Debug.Log("[SERVER -> ERROR] Champs vides");
                }

                if (response == 402)
                {
                    Debug.Log("[SERVER -> ERROR] Participant pas dans la bonne partie");
                }

                if (response == 403)
                {
                    Debug.Log("[SERVER -> ERROR] Participant non-existant");
                }

                if (response == 404)
                {
                    Debug.Log("[SERVER -> ERROR] Partie non-existante");
                }

                if (response == 200)
                {
                    Debug.Log("[SERVER -> SUCCESS] Tout est bien");
                }
            }
        }
    }

    public IEnumerator GetPlayerID(Player p)
    {
        WWWForm form = new WWWForm();
        form.AddField("login", p.playerName);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/conversion_login_to_id.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);
                Debug.Log(response);
                p.ID = response;
            }
        }
    }

    public IEnumerator SendDataToServer(int[] participants, int partie)
    {
        // Encode les données sous la forme d'une chaîne JSON
        string data = JsonUtility.ToJson(new DataPartie(participants, partie));

        //afficher le json 
        Debug.Log(data);

        // Envoie la requête POST au script PHP local
        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/recup_partie_info.php", data))
        {
            www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(data));
            www.downloadHandler = new DownloadHandlerBuffer();

            yield return www.SendWebRequest();

            // Vérifie s'il y a eu une erreur
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Affiche la réponse du serveur
                Debug.Log("Received: " + www.downloadHandler.text);
            }   
        }
    }
    
    public IEnumerator SendEvenementToServer(string[] evenements, int partie)
    {
        // Encode les données sous la forme d'une chaîne JSON
        string data = JsonUtility.ToJson(new EvenementPartie(evenements, partie));

        //afficher le json 
        Debug.Log(data);

        // Envoie la requête POST au script PHP local
        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/recup_evenement_test_1.php", data))
        {
            www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(data));
            www.downloadHandler = new DownloadHandlerBuffer();

            yield return www.SendWebRequest();

            // Vérifie s'il y a eu une erreur
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                // Affiche la réponse du serveur
                Debug.Log("Received: " + www.downloadHandler.text);
            }   
        }
    }


    
    public IEnumerator GetClassement(Player p)
    {
        WWWForm form = new WWWForm();
        form.AddField("login", p.playerName);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/login_to_classement.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);
                Debug.Log($"Classement for player : {p.playerName} retrieved");
                p.classement = response;
            }
        }
    }

    public List<Leaderboard> leaderboardList = new();
    
    public void initField(){
        for (int i = 0; i < 10; i++)
        {
            leaderboardList.Add(new());
            leaderboardList[i].login = " ";
            leaderboardList[i].best_score = new int();
        }
    }

    public GenLeaderboard gL;
    
    public IEnumerator GetLeaderboard(Player p)
    {
        using (UnityWebRequest www = UnityWebRequest.Get("192.168.100.104/top_10.php"))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                initField();
                string json = www.downloadHandler.text;
                gL = JsonUtility.FromJson<GenLeaderboard>(json);
                for (int i = 0; i < gL.classement.Length; i++)
                {
                    p.leaderboard.Add(gL.classement[i]);
                }

                Debug.Log("Retrieving leaderboard");
            }
        }
    }

    public IEnumerator FinDeLaPartie(int partie)
    {
        WWWForm form = new WWWForm();
        form.AddField("partie", partie);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/fin_partie.php", form))
        {

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 200)
                {
                    Debug.Log("[SERVER -> SUCCESS] Fin de partie enregistré");
                }

                if (response == 400)
                {
                    Debug.Log("[SERVER -> ERROR] Erreur lors de l'insertion ");
                }

                if (response == 401)
                {
                    Debug.Log("[SERVER -> ERROR] Champs vides");
                }

                if (response == 402)
                {
                    Debug.Log("[SERVER -> ERROR] La partie n'existe pas");
                }
            }
        }

    }
    
    public IEnumerator DisconnectPlayer(Player p)
    {
        Debug.Log("Enter player disconnect");
        
        WWWForm form = new WWWForm();
        form.AddField("username", p.playerName);

        using (UnityWebRequest www = UnityWebRequest.Post("192.168.100.104/disconnect.php", form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 200)
                {
                    Debug.Log("[SERVER -> SUCCESS] Disconnected with success.");
                }

                if (response == 401)
                {
                    Debug.Log("[SERVER -> ERROR] Champs vide.");
                }

                if (response == 400)
                {
                    Debug.Log("[SERVER -> ERROR]La requete a echoue.");
                }
            }
        }
    }
}
