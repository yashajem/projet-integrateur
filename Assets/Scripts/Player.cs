using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;

public struct ClientMessage : NetworkMessage
{
    public Player p;
    public Territory P_currentTerritory;
    public string answer;
    public bool answered;
    public string type;
    public float valTimer;
}

public class Player : NetworkBehaviour
{  
    /* ADEL */
    
    public static Player localPlayer;

    /* ALEX */

    [Header("General Player Info")]
    [SyncVar] public int ID;
    [SyncVar] public string playerName;
    [SyncVar] public int classement;


    [SyncVar(hook = nameof(OnChangedState))]public bool isAtacking;
    [SyncVar(hook = nameof(OnChangedIsBeingAttacked))] public bool isBeingAttacked;
    [SyncVar(hook = nameof(OnChangedBool))]public bool canAnswer;
    [SyncVar(hook = nameof(OnChangedAnsweredBool))]public bool p_answered;
    [SyncVar(hook = nameof(OnChangedString))]public string P_answer;
    
    
    [SerializeField]private GameObject canvas;
    public Territory currentTryTerritory;
    public TMP_Text phaseText;

    [Header("In-game Info")]
    [SyncVar(hook = nameof(OnChangedPoints))]public int points;
    [SyncVar(hook = nameof(OnChangedColor))]public Color playerColor;
    [SyncVar(hook = nameof(OnChangedTurn))]public bool myTurn;
    [SyncVar(hook = nameof(OnChangedTimer))]public float timeLeft;
    [SyncVar(hook = nameof(OnChangedQuestionCounter))] public int questionCounter;
    [SyncVar(hook = nameof(OnChangedCorrectQuestionsCounter))] public int corretQuestionCounter;


    public readonly SyncList<Territory> territoryList = new SyncList<Territory>();
    public readonly SyncList<Territory> selectedTerritoryExpansion = new SyncList<Territory>();
    public readonly SyncList<Territory> neighbours = new SyncList<Territory>();

    
    public bool timerOn = false;
    public bool inGame;
    private bool execOnce = false;
    public bool qna;
    public bool qnaRapid;
    public int numberPlayers = 0;

    public SyncList<Leaderboard> leaderboard = new SyncList<Leaderboard>();

    public TMP_Text timerText;
    public TMP_Text timerTextRapid;
    
    [SyncVar(hook = nameof(OnPlayerDataRetrieved))]
    bool _playerDataRetrieved;

    private bool getClassementOnce = false;

    /* ADEL */
    
    //Syncvar bc will be displayed on all version of this client
    [SyncVar]public string MatchID;

    //Utilisée dans UIPlayer pour l'affichage des "Player .." dans le lobby
    [SyncVar] public int playerIndex;
    NetworkMatch networkMatch;
    public GameObject manager;
    public Camera mainCamera;
    
    public Sprite[] ImagesTMP;

    public GameObject[] WaitingTMP;
    public readonly SyncList<int> ImagesIndex = new SyncList<int>();

    [SyncVar(hook = nameof(OnChangedPhase))] public GamePhase phase;

    [SyncVar(hook = nameof(IsCorrectAnswer))]
    public bool hadCorrectAnswer;

    [SyncVar(hook = nameof(ShouldHaveResponded))]
    public bool shouldHaveResponded;

    void Awake()
    {
        networkMatch = GetComponent<NetworkMatch>();
    }
    

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        manager = GameObject.FindGameObjectWithTag("Network");
        NetworkClient.UnregisterHandler<TriviareignAuthenticator.AuthResponseMessage>();
        NetworkClient.RegisterHandler<ServerMessage>(OnReceive);
        playerName = manager.GetComponent<TriviareignAuthenticator>().username;
        SetupPlayer(playerName);
        UILobby.instance.SetNickname(this);
        if (!string.IsNullOrEmpty(MatchID))
        {
            DisbandLobby();            
        }
        CmdGetPlayerID(this);
    }

    private IEnumerator GetPlayerDataCoroutine(Player p, ServerSideScripts ss)
    {
        yield return StartCoroutine(ss.GetPlayerID(p));
        yield return StartCoroutine(ss.GetClassement(p));
        yield return StartCoroutine(ss.GetLeaderboard(p));
    
        // Update the SyncVar to signal that the coroutines have finished retrieving data from the database
        _playerDataRetrieved = true;
    }
    
    private void OnPlayerDataRetrieved(bool oldValue, bool newValue)
    {
        if (newValue)
        {
            UILobby.instance.SetRang(this);
            UILobby.instance.SpawnLeaderboard(this);
        }
    }

    #region Server commands

    [Command]
    void CmdChangeCanAnswer(bool value)
    {
        canAnswer = value;
    }
    
    [Command]
    public void CmdIsAttacking(bool value)
    {
        isAtacking = value;
    }

    [Command]
    public void CmdAnswered(bool value)
    {
        p_answered = value;
    }

    [Command]
    public void CmdSetTimer(float value)
    {
        timeLeft = value;
    }

    [Command]
    public void CmdIsBeingAttacked(bool value)
    {
        Debug.Log($"Setting Is being attacked to {value} for player {playerName}");
        isBeingAttacked = value;
    }
    
    [Command]
    void CmdGetPlayerID(Player p)
    {
        ServerSideScripts ss = new ServerSideScripts();
        StartCoroutine(GetPlayerDataCoroutine(p, ss));
    }

    [Command]
    void SetupPlayer(string name_)
    {
        playerName = name_;
    }
    
    [Command]
    public void Disconnect()
    {
        if (inGame)
        {
            Debug.Log($"{playerName} was in game. He left. Removing him from players that play.");
            GameManager.instance.players.Remove(this);
            GameManager.instance.playersThatPlay.Remove(this);
        }
        ServerSideScripts ss = new ServerSideScripts();
        StartCoroutine(ss.DisconnectPlayer(this));
    }

    #endregion
    
    
    public void OnReceive(ServerMessage sMsg)
    {
        Debug.Log($"[CLIENT RECEIVED] StartQnA: {sMsg.startQnA} | StartQnARapid : {sMsg.startQnARapid} | Draw : {sMsg.draw} | Fin : {sMsg.fin}");
        
        timerText = GameObject.FindGameObjectWithTag("TimerText").GetComponent<TMP_Text>();
        timerTextRapid = GameObject.FindGameObjectWithTag("TimerTextRapid").GetComponent<TMP_Text>();

        if (sMsg.phase == GamePhase.battle_3)
        {
            UIGame.instance.fortificationButton.interactable = true;
            UIGame.instance.fortificationButton.gameObject.SetActive(true);
        }
        
        if (sMsg.fin)
        {
            if (sMsg.winner)
            {
                //Afficher canvas 
                MoveCamera(200,200,200);
                GameObject.FindGameObjectWithTag("PageDefaite").SetActive(false);
                GameObject.FindGameObjectWithTag("TerritoryCountText").GetComponent<TMP_Text>().text =
                    territoryList.Count.ToString();

                GameObject.FindGameObjectWithTag("PointsCountText").GetComponent<TMP_Text>().text = points.ToString();
                GameObject.FindGameObjectWithTag("ClassementText").GetComponent<TMP_Text>().text =
                    classement.ToString();
                GameObject.FindGameObjectWithTag("CanvasFinPartie").GetComponent<Canvas>().enabled = true;
            }
            else
            {
                MoveCamera(500,500,500);
                GameObject.FindGameObjectWithTag("PageVictoire").SetActive(false);
                GameObject.FindGameObjectWithTag("TerritoryCountTextDefaite").GetComponent<TMP_Text>().text =
                    territoryList.Count.ToString();

                GameObject.FindGameObjectWithTag("PointsCountTextDefaite").GetComponent<TMP_Text>().text = points.ToString();
                GameObject.FindGameObjectWithTag("ClassementTextDefaite").GetComponent<TMP_Text>().text =
                    classement.ToString();

                GameObject.FindGameObjectWithTag("CanvasFinPartie").GetComponent<Canvas>().enabled = true;
            }
        }
        
        if (sMsg is {draw: true, startQnARapid: true})
        {
            if (territoryList.Contains(sMsg.currentTer) && sMsg.phase == GamePhase.battle_3)
            {
                isBeingAttacked = true;
                CmdIsBeingAttacked(true);
            }
            EnableAllButtonsQR();
            qna = false;
            qnaRapid = true;
            CmdMoveCamera(200, 200,200);
            MoveCamera(200,200,200);
            CmdSetTimer(20f);
            timeLeft = 20f;
            timerOn = true;
            Debug.Log($"Valeur timer : {timerOn}");
            Debug.Log($"Client received Territory : {sMsg.currentTer} Phase : {sMsg.phase} ");
            //Start question de rapidite, show canvas rapidite
            if ((myTurn || territoryList.Contains(sMsg.currentTer)) && sMsg.phase == GamePhase.battle_3)
            {
                CmdChangeCanAnswer(true);
            }
            else if (sMsg.phase == GamePhase.expantion_21 || sMsg.phase == GamePhase.allocation_22)
            {
                CmdChangeCanAnswer(true);
            }
            else
            {
                isBeingAttacked = false;
                CmdIsBeingAttacked(false);
                CmdChangeCanAnswer(false);
            }
            
            ShowCanvasQR(sMsg.qR.Question);
            return;
        }
        
        currentTryTerritory = sMsg.currentTer;
        
        switch (sMsg.startQnA)
        {
            case true:
            {
                if (territoryList.Contains(sMsg.currentTer) && sMsg.phase == GamePhase.battle_3)
                {
                    isBeingAttacked = true;
                    CmdIsBeingAttacked(true);
                }
                qna = true;
                qnaRapid = false;
                CmdMoveCamera(200,200,200);
                MoveCamera(200,200,200);
                //Start timer => IN UPDATE : if timer = 0 and p_answer false => network send
                CmdSetTimer(20f);
                timeLeft = 20f;
                timerOn = true;
                Debug.Log($"Valeur timer : {timerOn}");
                EnableAllButtons();

                if ((myTurn || territoryList.Contains(sMsg.currentTer)) && sMsg.phase == GamePhase.battle_3)
                {
                    CmdChangeCanAnswer(true);
                }
                else if (sMsg.phase == GamePhase.expantion_21)
                {
                    CmdChangeCanAnswer(true);
                }
                else
                {
                    isBeingAttacked = false;
                    CmdIsBeingAttacked(false);
                    CmdChangeCanAnswer(false);
                }
            
                Debug.Log("Client received: " + sMsg.currentTer);

                string question = sMsg.q.Question;
            
                ShowCanvas(question);

                GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonResponse");

                for (int i = 0; i < buttons.Length; i++)
                {
                    buttons[i].transform.GetChild(0).GetComponent<TMP_Text>().text = sMsg.q.Answers[i];
                }
                
                break;
            }
            case false when sMsg.startQnARapid == false:
                qna = false;
                qnaRapid = false;
                CmdMoveCamera(0,0,-10);
                MoveCamera(0,0,-10);
                CmdUpdateTurn();
                CmdUpdatePoints();
                timerOn = false;
                Debug.Log($"Valeur timer : {timerOn}");
                HideCanvas();
                HideCanvasQR();
                CmdChangeCanAnswer(false);
                CmdAnswered(false);
                P_answer = "";
                currentTryTerritory = null;
                CmdIsAttacking(false);
                CmdIsBeingAttacked(false);
                isAtacking = false;
                isBeingAttacked = false;
                break;
        }
    }

    #region Camera movement

    [Command]
    public void CmdMoveCamera(float x, float y, float z)
    {
        RpcMoveCamera(x, y, z);
    }

    [ClientRpc]
    void RpcMoveCamera(float x, float y, float z)
    {
        MoveCamera(x,y,z);
    }

    void MoveCamera(float x, float y, float z)
    {
        Debug.Log($"Moving camera to {x} {y} {z}");
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mainCamera.transform.position = new Vector3(x, y, z);
    }

    #endregion

    #region ShowCanvas

    void ShowCanvas(string question)
    {
        timerOn = true;
        MoveCamera(200,200,200);
        canvas = GameObject.FindWithTag("QuestionCanvas");
        canvas.transform.GetChild(0).transform.GetChild(0).GetComponent<TMP_Text>().text = question;

        canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "";
        Debug.Log($"Before if. Is being attacked? {isBeingAttacked}");
        if (isBeingAttacked)
        {
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "Tu te fais attaqué. Defends ton territoire!";
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().color = Color.red;
        }
        else if (isAtacking)
        {
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "Tu es en train d'attaquer!";
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().color = Color.blue;
        }
        else if (phase is not (GamePhase.expantion_21 or GamePhase.allocation_22) || (isAtacking == false && isBeingAttacked == false && phase is GamePhase.battle_3))
        {
            DisableAllButtons();
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().text = "Vous n'avez rien a faire! Preparez-vous pour le prochain tour!";
            canvas.transform.GetChild(0).transform.GetChild(1).GetComponent<TMP_Text>().color = Color.black; 
        }
        Debug.Log("After if");
        canvas.GetComponent<Canvas>().enabled = true;
    }
    
    void ShowCanvasQR(string question)
    {
        timerOn = true;
        CmdAnswered(false);
        HideCanvas();
        MoveCamera(200,200,200);
        timerOn = true;
        canvas = GameObject.FindWithTag("QuestionRapiditeCanvas");
        GameObject.FindWithTag("ResponseInput").GetComponent<TMP_Text>().text = "";
        GameObject.FindWithTag("QuestionText").GetComponent<TMP_Text>().text = question;
        Debug.Log("Before if rapid");
        GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().text =
            "";
        if (isBeingAttacked)
        {
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().text =
                "Tu te fais attaqué. Defends ton territoire!";
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().color =
                Color.red;        
        }
        else if (isAtacking)
        {
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().text =
                "Tu es en train d'attaquer!";
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().color =
                Color.blue;
        }
        else if (phase is not (GamePhase.expantion_21 or GamePhase.allocation_22) || (isAtacking == false && isBeingAttacked == false && phase is GamePhase.battle_3))
        {
            DisableAllButtonsQR();
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().text =
                "Vous n'avez rien a faire! Preparez vous pour le prochain tour!";
            GameObject.FindGameObjectWithTag("AttackTextRapid").GetComponent<TMP_Text>().color =
                Color.black;
        }

        Debug.Log("After if rapid");
        canvas.GetComponent<Canvas>().enabled = true;
    }


    #endregion

    #region Fonctions Affichage 

        
    IEnumerator DelayedSpawnUI(List<Player> p)
    {
        yield return new WaitForSeconds(1f);
        foreach (var playerUI in p)
        {
            UIGame.instance.SpawnPlayerUIPrefab(playerUI);   
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));
    }

    [ClientRpc]
    void TargetGameStarted(List<Player> p)
    {
        StartCoroutine(DelayedSpawnUI(p));
    }
     
    [Command]
    public void CmdUpdatePoints()
    {
        //Go through all the players, check their values and for each thingy, change the value to current value
        foreach (var player in GameManager.instance.players)
        {
            RpcUpdatePoints(player);
        }
    }

    [ClientRpc]
    void RpcUpdatePoints(Player p)
    {
        foreach (var t in FindObjectsOfType<GameUIPlayer>())
        {
            if (t.username.text == p.playerName)
            {
                t.points.text = p.points.ToString();
            }
        }
    }

    [Command]
    public void CmdUpdateTurn()
    {
        foreach (var player in GameManager.instance.players)
        {
            RpcUpdateTurn(player);
        }
    }

    [ClientRpc]
    public void RpcUpdateTurn(Player p)
    {
        foreach (var t in FindObjectsOfType<GameUIPlayer>())
        {
            if (t.username.text == p.playerName)
            {
                if (t.player.myTurn)
                {
                    t.turn.text = "C'est ton tour!";
                    t.turn.color = Color.green;
                }
                else
                {
                    t.turn.text = $"Ce n'est pas ton tour!";
                    t.turn.color = Color.red;
                }
            }
        }
    }
    
    IEnumerator FadeOut()
    {
        float fadeDuration = 5f;
        // Get the initial color of the text
        Color originalColor = UIGame.instance.goodResponse.color;

        // Loop until the text is completely faded out
        float timeElapsed = 0f;
        while (timeElapsed < fadeDuration) {
            // Calculate the current alpha value based on the elapsed time
            float alpha = Mathf.Lerp(1f, 0f, timeElapsed / fadeDuration);

            // Update the text color with the new alpha value
            Color newColor = new Color(originalColor.r, originalColor.g, originalColor.b, alpha);
            UIGame.instance.goodResponse.color = newColor;

            // Wait for the next frame
            yield return null;

            // Update the elapsed time
            timeElapsed += Time.deltaTime;
        }

        // Set the final color to be fully transparent
        UIGame.instance.goodResponse.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0f);
    }
    
    IEnumerator ShowResponse()
    {
        yield return new WaitForSeconds(0.2f);
        
        if (shouldHaveResponded == false)
        {
            UIGame.instance.goodResponse.text = "";
        }
        
        if (hadCorrectAnswer == true && phase is GamePhase.battle_3 && shouldHaveResponded)
        {
            UIGame.instance.goodResponse.text = "Vous avez bien repondu!";
            UIGame.instance.goodResponse.color = Color.Lerp(Color.green, new Color(0f, 1f, 0.5f), 0.5f);
            StartCoroutine(FadeOut());
        }
        
        if (hadCorrectAnswer == false && shouldHaveResponded && phase is GamePhase.battle_3)
        {
            UIGame.instance.goodResponse.text = "Vous avez mal répondu!";
            UIGame.instance.goodResponse.color = Color.red;
            StartCoroutine(FadeOut());
        }
    }

    #endregion
    
    #region HideCanvas
    void HideCanvasQR()
    {
        timerOn = false;
        canvas = GameObject.FindWithTag("QuestionRapiditeCanvas");
        canvas.GetComponent<Canvas>().enabled = false;

        UIGame.instance.goodResponse.text = "";

        StartCoroutine(ShowResponse());
    }

    void HideCanvas()
    {
        timerOn = false;
        canvas = GameObject.FindWithTag("QuestionCanvas");
        canvas.GetComponent<Canvas>().enabled = false;
        
        UIGame.instance.goodResponse.text = "";

        StartCoroutine(ShowResponse());
    }

    #endregion

    #region SendNothing

    void SendNothingQR()
    {
        if (!NetworkClient.connection.identity.GetComponent<Player>().p_answered &&
            NetworkClient.connection.identity.GetComponent<Player>().canAnswer)
        {
            NetworkClient.Send(new ClientMessage
            {
                p = NetworkClient.connection.identity.GetComponent<Player>(),
                answer = "0",
                type = "rapid",
                answered = true,
                P_currentTerritory = GetComponent<NetworkIdentity>().GetComponent<Player>().currentTryTerritory
            });
        }
    }
    
    void SendNothing()
    {
        if (!NetworkClient.connection.identity.GetComponent<Player>().p_answered &&
            NetworkClient.connection.identity.GetComponent<Player>().canAnswer)
        {
            NetworkClient.Send(new ClientMessage
            {
                p = NetworkClient.connection.identity.GetComponent<Player>(),
                answer = "0",
                type = "simple",
                answered = true,
                P_currentTerritory = GetComponent<NetworkIdentity>().GetComponent<Player>().currentTryTerritory
            });
        }
    }

    #endregion

    private void Update()
    {
        if (inGame)
        {
            UIGame.instance.phaseText.text = phase switch
            {
                GamePhase.expantion_21 => "Phase : Expansion",
                GamePhase.battle_3 => "Phase : Bataille",
                _ => UIGame.instance.phaseText.text
            };
        }

        if (execOnce == false)
        {
            if (inGame)
            {
                CmdGameStarted();
                
                CmdUpdateTurn();
                execOnce = true;
            }
        }
        
        if (timerOn)
        {
            if (timeLeft > 0)
            {
                timeLeft = Math.Max(0, timeLeft - Time.deltaTime);
                CmdSetTimer(timeLeft);

                if (qna)
                {
                    int seconds = Mathf.FloorToInt(timeLeft % 60);
                    timerText.text = "0:" + seconds.ToString("00");
                }

                if (qnaRapid)
                {
                    int seconds = Mathf.FloorToInt(timeLeft % 60);
                    timerTextRapid.text = "0:" + seconds.ToString("00");
                }
            }
            else
            {
                timeLeft = 0;
                if (qna)
                {
                    SendNothing();
                }

                if (qnaRapid)
                {
                    SendNothingQR();
                }
                timerOn = false;
            }
        }
    }

    #region Enable/Disable buttons

    void DisableAllButtons()
    {
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonResponse");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Button>().interactable = false;
        }
    }
    
    void EnableAllButtons()
    {
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonResponse");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Button>().interactable = true;
        }
    }

    void DisableAllButtonsQR()
    {
        GameObject.FindWithTag("ButtonValider").GetComponent<Button>().interactable = false;
        GameObject.FindWithTag("ButtonEffacer").GetComponent<Button>().interactable = false;
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonResponseRapidite");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Button>().interactable = false;
        }
    }
    
    void EnableAllButtonsQR()
    {
        GameObject.FindWithTag("ButtonValider").GetComponent<Button>().interactable = true;
        GameObject.FindWithTag("ButtonEffacer").GetComponent<Button>().interactable = true;
        GameObject[] buttons = GameObject.FindGameObjectsWithTag("ButtonResponseRapidite");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<Button>().interactable = true;
        }
    }

    #endregion

    #region SendAnswer

    public void SendAnswerQR()
    {
        NetworkIdentity nId = NetworkClient.connection.identity;
        
        nId.GetComponent<Player>().timerOn = false;

        string response = GameObject.FindWithTag("ResponseInput").GetComponent<TMP_Text>().text;

        if (nId.GetComponent<Player>().canAnswer)
        {
            nId.GetComponent<Player>().CmdAnswered(true);
            NetworkClient.Send(new ClientMessage
                {
                    p = nId.transform.GetComponent<Player>(),
                    answer = response,
                    type = "rapid",
                    answered = true,
                    valTimer = timeLeft,
                    P_currentTerritory = nId.GetComponent<Player>().currentTryTerritory
                }
            );
            DisableAllButtonsQR();
        }
    }
    
    public void SendAnswer()
    {
        NetworkIdentity nId = NetworkClient.connection.identity;

        nId.GetComponent<Player>().timerOn = false;

        string response = EventSystem.current.currentSelectedGameObject
            .transform.GetChild(0).GetComponent<TMP_Text>().text;

        Debug.Log(response);
        
        if (nId.GetComponent<Player>().canAnswer)
        {
            nId.GetComponent<Player>().CmdAnswered(true);
            NetworkClient.Send(new ClientMessage{
                p = nId.transform.GetComponent<Player>(), 
                answer = response, 
                type = "simple",
                answered = true,
                P_currentTerritory = nId.GetComponent<Player>().currentTryTerritory});
            DisableAllButtons();
        }
    }

    #endregion
    
    
    //Quand un autre joueur rejoint le lobby, Start est rappelée
    //Si ce n'est pas le localplayer, il va quand même appelée Spawn..
    public override void OnStartClient()
    {
        if (isLocalPlayer) {
            localPlayer = this;
        }
        else {
            UILobby.instance.SpawnPlayerUIPrefab(this);
        }
    }


    #region Lobby functions

    public void HostGame() {
        string matchID = MatchMaker.GetRandomMatchId();
        CmdHostGame(matchID);
    }
    
    //Player dit au serveur d'exécuter cette fonction
    //Le serveur va ajouter cet id à la liste des matchs existant
    [Command] 
    void CmdHostGame(string matchID) {
        MatchID = matchID;
        //out directement fait ref à playerIndex
        if(MatchMaker.instance.HostGame(matchID, this, out playerIndex)) {
            Debug.Log("Game hosted successfully !");
            networkMatch.matchId = matchID.ToGuid();
            TargetHostGame(true, matchID, playerIndex);
        }
        else 
            Debug.Log("Game hosted failed");
        TargetHostGame(false, matchID, playerIndex);
    }

    //Le client concerné appelle cette fonction
    [TargetRpc]
    void TargetHostGame(bool success, string matchID, int _playerIndex) {
        playerIndex = _playerIndex;
        Debug.Log("Match ID : " + matchID + " == " + MatchID);
        UILobby.instance.HostSuccess(success);
    }

    public void JoinGame(string matchID) {
        CmdJoinGame(matchID);
    }

    //Player dit au serveur d'exécuter cette fonction
    //Le serveur va ajouter cet id à la liste des matchs existant
    [Command] 
    void CmdJoinGame(string matchID) {
        MatchID = matchID;
        if(MatchMaker.instance.JoinGame(matchID, this, out playerIndex)) {
            Debug.Log("Game joined successfully !");
            networkMatch.matchId = matchID.ToGuid();
            TargetJoinGame(true, matchID, playerIndex);
        }
        else 
            Debug.Log("Game joined failed");
        TargetJoinGame(false, matchID, playerIndex);
    }

    //Le client concerné appelle cette fonction
    [TargetRpc]
    void TargetJoinGame(bool success, string matchID, int _playerIndex) {
        playerIndex = _playerIndex;
        Debug.Log("Match ID : " + matchID + " == " + MatchID);
        UILobby.instance.JoinSuccess(success);
    }

    [Command]
    public void CmdUpdateNumberPlayers(int numberPlayersTmp) {
        RpcUpdateNumberPlayers(numberPlayersTmp);
    }

    [ClientRpc]
    public void RpcUpdateNumberPlayers(int numberPlayersTmp) {
        numberPlayers = numberPlayersTmp;
        string numberPlayersText = "NOMBRE DE JOUEURS : " + numberPlayers + "/10";

        UILobby.instance.numberPlayersTxt.text = numberPlayersText;

        //Quand assez de joueurs sont dans le lobby, l'hôte peut lancer la partie
        if (numberPlayers >= 3) {
            UILobby.instance.startGame_button.interactable = true;
        }
    }
    
    
    [Command]
    public void SetAvatarList() {

        /*for (int i = 0; i < ImagesTMP.Length; i++) {
            ImagesIndex.Add(i);
        }*/

        RpcSetUIPlayers(); 
    }

    [ClientRpc]
    public void RpcSetUIPlayers() {
        //WaitingTMP = GameObject.FindGameObjectsWithTag("WaitingPlayersUI");

        for (int i = 0; i < WaitingTMP.Length; i++)
            Debug.Log("Waiting de Players : " + WaitingTMP[i]);
            
        for (int i = 0; i < UILobby.instance.PlayerUIList.Count; i++) {
            //int random_index = Random.Range(0, ImagesTMP.Length);
            //UILobby.instance.PlayerUIList[i].GetComponentInChildren<Image>().sprite = ImagesTMP[random_index];
            UILobby.instance.PlayerUIList[i].GetComponentsInChildren<Image>()[1].sprite = ImagesTMP[i];
        }

        Destroy(UILobby.instance.WaitingTMP[playerIndex - 1]);
    }

    /* -------- */

    public void StartGame() {
        CmdStartGame();
    }

    public void BeginGame() {
     TargetStartGame();
    }

    [Command]
    void CmdGameStarted()
    {
     List<Player> playersUI = new List<Player>();
     foreach (var player in GameManager.instance.players)
     {
         playersUI.Add(player);
     }
     TargetGameStarted(playersUI);
    }

    public void DisbandLobby() {
     CmdDisbandLobby();
    }

    [Command]
    public void CmdDisbandLobby() {
     MatchMaker.instance.DisbandLobby(MatchID);
     RpcDisbandLobby();
    }

    [ClientRpc]
    public void RpcDisbandLobby() {
     UILobby.instance.DisbandLobbyChangeUI();
    }


    //Le client concerné appelle cette fonction
    //Pas clientrpc parce qu'on veut pas tous les clients qui load la scène
    [TargetRpc]
    void TargetStartGame() {
     Debug.Log("Match ID : " + MatchID + " beginning");
     //FindObjectOfType<UILobby>().gameObject.SetActive(false);
    }

         /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le joueur va demander au serveur d'executer cette fonction.
                    Responsable pour le lancement d'une partie.
                    Le serveur va ajouter cet id à la liste des matchs existant.                                      */
    /*----------------------------------------------------------------------------------------------------------------*/
    [Command] 
    void CmdStartGame() {
        MatchMaker.instance.StartGame(MatchID);
        Debug.Log($"[INFO -> SUCCESS] | [MATHID : {MatchID}] Game Starting");
        ChangeScene();
    }

    public void EndGame()
    {
        CmdEndGame();
    }
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le joueur va demander au serveur d'executer cette fonction.
                    Responsable pour le fin d'une partie. La partie est finie => on va dissoudre le lobby.            */
    /*----------------------------------------------------------------------------------------------------------------*/
    [Command]
    public void CmdEndGame()
    {
        MatchMaker.instance.RemovePlayerFromMatch(this, MatchID);
        if (GameManager.instance != null && GameManager.instance.gameObject != null)
        {
            NetworkServer.Destroy(GameManager.instance.gameObject);
        }
        
        myTurn = false;
        hadCorrectAnswer = false;
        shouldHaveResponded = false;
        P_answer = null;
        canAnswer = false;
        isAtacking = false;
        isBeingAttacked = false;
        p_answered = false;
        points = 0;
        playerColor = Color.black;
        MatchID = null;
        timeLeft = 0;
        corretQuestionCounter = 0;
        territoryList.Clear();
        neighbours.Clear();
        selectedTerritoryExpansion.Clear();
        ClearAndMove();
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le serveur va demander au joueur d'executer cette fonction.
                    Les joueurs concernées vont clear leur données et ils vont changer de scene.                      */
    /*----------------------------------------------------------------------------------------------------------------*/
    [TargetRpc]
    public void ClearAndMove()
    {
        currentTryTerritory = null;
        timerOn = false;
        inGame = false;
        execOnce = false;
        qna = false;
        qnaRapid = false;
        numberPlayers = 0;

        SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByBuildIndex(1));
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(1));
        //FindObjectOfType<UILobby>().gameObject.SetActive(true);
        UILobby.instance.SetMenuActive();
        
        SceneManager.UnloadSceneAsync(SceneManager.GetSceneByBuildIndex(2));
        UILobby.instance.DisbandLobbyChangeUI();
    }
    
    /* ALEX */

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le serveur va dire au joueur d'executer cette methode.
                    Elle est responsable pour changer la scene et deplacer tous les objets dans la scene game.        */
    /*----------------------------------------------------------------------------------------------------------------*/
    [ClientRpc]
    void ChangeScene()
    {
        Debug.Log("[INFO] | Loading Scene");

        LoadScene();
    }

    public async void LoadScene()
    {
        var scene = SceneManager.LoadSceneAsync(2, new LoadSceneParameters(LoadSceneMode.Additive));
        scene.allowSceneActivation = false;

        do { await UniTask.Delay(100); } while (scene.progress < 0.9f);

        scene.allowSceneActivation = true;
        
        //Transferer les territoires dans la scene Game
        foreach (var ter in GameObject.FindGameObjectsWithTag("Territory"))
        {
            SceneManager.MoveGameObjectToScene(ter.gameObject, SceneManager.GetSceneByBuildIndex(2));
        }
        
        //Transferer le player dans la scene Game
        SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetSceneByBuildIndex(2));
        
        inGame = true;
        timerOn = false;
        timeLeft = 20f;
        qna = false;
        qnaRapid = false;
        isBeingAttacked = false;
        isAtacking = false;
        shouldHaveResponded = false;
    }

     
    #endregion
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Comme tous les variables sont des SyncVars on est obligé d'avoir des fonctions de synchronization.*/
    /*----------------------------------------------------------------------------------------------------------------*/
    #region DataSynchronization
    
    void OnChangedPoints(int oldValue, int newValue)
    {
        points = newValue;
    }
    
    void OnChangedPhase(GamePhase oldValue, GamePhase newValue)
    {
        phase = newValue;
    }

    void IsCorrectAnswer(bool oldVal, bool newVal)
    {
        hadCorrectAnswer = newVal;
    }

    void ShouldHaveResponded(bool oldVal, bool newVal)
    {
        shouldHaveResponded = newVal;
    }
    
    void OnChangedQuestionCounter(int oldValue, int newValue)
    {
        questionCounter = newValue;
    }

    void OnChangedCorrectQuestionsCounter(int oldValue, int newValue)
    {
        corretQuestionCounter = newValue;
    }

    void OnChangedTurn(bool oldValue, bool newValue)
    {
        myTurn = newValue;
    }

    void OnChangedIsBeingAttacked(bool oldValue, bool newValue)
    {
        isBeingAttacked = newValue;
    }

    void OnChangedState(bool oldValue, bool newValue)
    {
        isAtacking = newValue;
    }
        
    void OnChangedBool(bool oldValue, bool newValue)
    {
        canAnswer = newValue;
    }

    void OnChangedTimer(float oldVal, float newVal)
    {
        timeLeft = newVal;
    }
    
    void OnChangedAnsweredBool(bool oldValue, bool newValue)
    {
        p_answered = newValue;
    }
    
    void OnChangedString(string oldValue, string newValue)
    {
        P_answer = newValue;
    }
    
    void OnChangedColor(Color oldValue, Color newValue)
    {
        playerColor = newValue;
    }
    
    #endregion
    
    #region Game Functions
    
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le joueur va demander au serveur d'executer le code suivant. Cette fonction va executer fonction
                    RpcShowTerritory (explications par la suite).  */
    /*----------------------------------------------------------------------------------------------------------------*/
    [Command]
    public void CmdUpdateData(Territory t)
    {
        if (myTurn && isClient)
        {
            RpcShowTerritory(t);
        }
    }
    

    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le joueur va demander au serveur d'executer le morceau de code suivant. Le serveur va verifier si
                    le joueur en question a dans sa liste des territoires le territoire qu'il veut conquerir, et si il
                    l'a pas, alors on va l'ajouter dans la liste. Comme ca, le joueur ET le serveur auront l'information
                    necessaire. */
    /*----------------------------------------------------------------------------------------------------------------*/
    [Command]
    public void CmdCaptureTerritory(Territory t)
    {
        if (GameManager.instance.gameStatus == GamePhase.baseSelect_1)
        {
            foreach (var player in GameManager.instance.players)
            {
                RpcUpdateTurn(player);
            }

            GameManager.instance.terDispo--;
            t.SetBase();
            GameManager.instance.playersWithBase.Add(this);
        }

        if (GameManager.instance.gameStatus == GamePhase.battle_3 || GameManager.instance.gameStatus == GamePhase.baseSelect_1)
        {
            foreach (var player in GameManager.instance.players)
            {
                RpcUpdateTurn(player);
            }
            Debug.Log("[DEBUG] Entered capture territory");
            currentTryTerritory = t;
            if (territoryList.Contains(t)) return;
            Debug.Log("Added " + t + " in players " + playerName + " territory list.");
            territoryList.Add(t);
            foreach (var neighbour in t.neighbours)
            {
                if (!neighbours.Contains(neighbour))
                {
                    neighbours.Add(neighbour);
                }
            }
        }
    }

    [Command]
    public void CmdSelectTerritory(Territory t)
    {
        Debug.Log($"[TERRITORY SELECTION PHASE] {playerName} selected {t}");
        foreach (var player in GameManager.instance.players)
        {
            RpcUpdateTurn(player);
        }
        selectedTerritoryExpansion.Add(t);
    }
    
    /*----------------------------------------------------------------------------------------------------------------*/
    /* [Purpose] :  Le serveur va demander au joueurs d'executer le code suivant. C'est comme une fonction de
                    synchronization entre les joueurs. Tout ce que la fonction fait c'est de changer la couleur 
                    du territoire pour tous les clients  */
    /*----------------------------------------------------------------------------------------------------------------*/
    [ClientRpc]
    public void RpcShowTerritory(Territory t)
    {
        if (t.isBase)
        {
            t.territoryColor = new Color(playerColor.r - 0.5f, playerColor.g - 0.5f, playerColor.b - 0.5f, 1f);
        }
        else
        {
            t.territoryColor = new Color(playerColor.r, playerColor.g, playerColor.b, 0.5f);
        }
    }

    #endregion
}

