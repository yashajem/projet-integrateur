using System.Collections;
using System.Collections.Generic;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIGame : MonoBehaviour
{
    public static UIGame instance;

    [SerializeField] Transform PlayersGrid;
    [SerializeField] GameObject GameUIPlayerPrefab;

    [SerializeField] public Button fortificationButton;

    public TMP_Text phaseText;
    public TMP_Text goodResponse;

    public Button finPartieVictoire;
    public Button finPartieDefaite;
    
    void Start() {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(GameObject.FindWithTag("MainCamera"));
            Destroy(GameObject.FindWithTag("AudioPrincipale"));
        }
        GetComponent<Canvas>().worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

        phaseText.text = "Phase : Selection de base";
        
        fortificationButton.onClick.AddListener(UpdateClickStatus);
        fortificationButton.interactable = false;
        fortificationButton.gameObject.SetActive(true);
        GameObject.FindGameObjectWithTag("QuitButton").gameObject.SetActive(false);
    }
    
    public void SpawnPlayerUIPrefab(Player player) {
        GameObject newUIPlayer = Instantiate(GameUIPlayerPrefab, PlayersGrid);
        newUIPlayer.GetComponent<GameUIPlayer>().SetPlayer(player);
        fortificationButton = GameObject.Find("FortificationButton").GetComponent<Button>();

        //Pour que les joueurs s'affichent dans le bon ordre dans le lobby
        newUIPlayer.transform.SetSiblingIndex(player.playerIndex - 1);
    }
    
    public void UpdateClickStatus()
    {
        Player p = NetworkClient.connection.identity.GetComponent<Player>();

        if(Territory.fortMode){
            Territory.fortMode = false;
            fortificationButton.GetComponent<Image>().color = Color.red;
            foreach (var ter in GameObject.FindGameObjectsWithTag("Territory"))
            {
                if (p.territoryList.Contains(ter.GetComponent<Territory>()))
                {
                    ter.GetComponent<Territory>().territoryColor.a = 0.3f;
                }
            }
        }
        else
        {
            fortificationButton.GetComponent<Image>().color = Color.green;
            foreach (var ter in GameObject.FindGameObjectsWithTag("Territory"))
            {
                if (p.territoryList.Contains(ter.GetComponent<Territory>()))
                {
                    ter.GetComponent<Territory>().territoryColor.a = 1f;
                }
            }
            Territory.fortMode = true;
        }
    }

    public void EndGame()
    {
        Player.localPlayer.EndGame();
    }
}
