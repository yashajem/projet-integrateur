using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Unity.VisualScripting;

public class UILobby : MonoBehaviour
{
    public static UILobby instance;
    
    [Header("Host Join")]
    //Rendre variables private visibles dans l'inspecteur
    [SerializeField] TMP_Text nickname;
    [SerializeField] TMP_Text rang;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject lobbyUI;
    [SerializeField] TMP_InputField joinInput;
    [SerializeField] Button joinButton;
    [SerializeField] Button hostButton;
    [SerializeField] Canvas lobbyCanvas;
    [SerializeField] TMP_Text errorText;
    
    
    [Header("Lobby")]
    [SerializeField] Transform UIPlayerParent;
    [SerializeField] GameObject UIPlayerPrefab;
    [SerializeField] TMP_Text matchID_text;
    public Button startGame_button;
    [SerializeField] Button disbandLobby_button;
    public TMP_Text numberPlayersTxt;

    [Header("Leaderboard")] 
    public Transform UIRowParent;
    public GameObject UIRowPrefab;
    


    [NonSerialized] public List<GameObject> PlayerUIList;

    public GameObject[] WaitingTMP;
    int numberPlayers = 0;

    bool canStartGame = false;
    
    void Start() {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(GameObject.FindWithTag("MainCamera"));
            Destroy(GameObject.FindWithTag("AudioPrincipale"));
        }
        PlayerUIList = new List<GameObject>();
        GetComponent<Canvas>().worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    public IEnumerator Waiter()
    {
        yield return new WaitForSeconds(5f);
        NetworkClient.connection.Disconnect();
    }
    
    public void Disconnect()
    {
        GameObject.FindWithTag("SonClick").GetComponent<AudioSource>().Play();
        /*NetworkManager n = GameObject.FindGameObjectWithTag("Network").GetComponent<NetworkManager>();
        n.StopClient();*/
        
        NetworkClient.connection.identity.GetComponent<Player>().Disconnect();
        
        StartCoroutine(Waiter());

        //NetworkManager.singleton.StopClient();
        //NetworkClient.Shutdown();
    }
    
    private IEnumerator Fade()
    {
        Color textColor = errorText.color;
        textColor.a = 0f;
        errorText.color = textColor;

        // Fade in
        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 3f;
            textColor.a = Mathf.Lerp(0f, 1f, t);
            errorText.color = textColor;
            yield return null;
        }

        yield return new WaitForSeconds(5f);

        // Fade out
        t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / 3f;
            textColor.a = Mathf.Lerp(1f, 0f, t);
            errorText.color = textColor;
            yield return null;
        }
    }

    // Méthode publique pour démarrer la coroutine de fondu
    public void StartFadeOut()
    {
        StartCoroutine(Fade());
    }
    

    public void Host() {
        //Quand on a appuyé sur le bouton on rend tous ces champs non interagissables
        GameObject.FindWithTag("SonClick").GetComponent<AudioSource>().Play();
        joinInput.interactable = false;
        joinButton.interactable = false;
        hostButton.interactable = false;

        Player.localPlayer.HostGame();
    }

    public void HostSuccess(bool success) {
        if (success) {
            lobbyUI.SetActive(true);
            mainMenu.SetActive(false);
            FindObjectOfType<SettingsMenu>().retourButton.gameObject.SetActive(false);
            FindObjectOfType<SettingsMenu>().quitButton.gameObject.SetActive(false);
            SpawnPlayerUIPrefab(Player.localPlayer);
            matchID_text.text = Player.localPlayer.MatchID;
            startGame_button.gameObject.SetActive(true);
            disbandLobby_button.gameObject.SetActive(true);
            if (disbandLobby_button.interactable == false) disbandLobby_button.interactable = true;
            startGame_button.interactable = false;
            numberPlayers = Player.localPlayer.playerIndex;
            lobbyCanvas.enabled = true;
            lobbyCanvas.overrideSorting = false;

            Player.localPlayer.CmdUpdateNumberPlayers(numberPlayers);
        }
        else {
            joinInput.interactable = true;
            joinButton.interactable = true;
            hostButton.interactable = true;
        }
    }

    public void Join() {
        //Quand on a appuyé sur le bouton on rend tous ces champs non interagissables
        GameObject.FindWithTag("SonClick").GetComponent<AudioSource>().Play();
        joinInput.interactable = false;
        joinButton.interactable = false;
        hostButton.interactable = false;

        //Le matchID = ce que tape l'utilisateur dans l'input field
        Player.localPlayer.JoinGame(joinInput.text);
    }

    public void JoinSuccess (bool success) {
        if (success) {
            lobbyCanvas.enabled = true;
            lobbyCanvas.overrideSorting = false;
            FindObjectOfType<SettingsMenu>().retourButton.gameObject.SetActive(false);
            FindObjectOfType<SettingsMenu>().quitButton.gameObject.SetActive(false);
            mainMenu.SetActive(false);
            //lobbyUI.SetActive(true);
            SpawnPlayerUIPrefab(Player.localPlayer);
            matchID_text.text = Player.localPlayer.MatchID;
            numberPlayers = Player.localPlayer.playerIndex;
            Player.localPlayer.CmdUpdateNumberPlayers(numberPlayers);
        }
        else { 
            joinInput.interactable = true;
            joinButton.interactable = true;
            hostButton.interactable = true;
            StartFadeOut();
        }
    }

    public void SetNickname(Player player)
    {
        Debug.Log("Set nickname");
        nickname.text = player.playerName;
    }

    public void SetRang(Player player)
    {
        Debug.Log("Set rang");
        if (string.IsNullOrEmpty(rang.text))
        {
            rang.text = player.classement.ToString();   
        }
        else Debug.Log("Already has rank");
    }
    
    public void SpawnLeaderboard(Player p)
    {
        if (UIRowParent.transform.childCount == 0)
        {
            int count = 0;
            foreach (var t in p.leaderboard)
            {
                count++;
                GameObject newUIPlayer = Instantiate(UIRowPrefab, UIRowParent);
                TMP_Text[] texts = newUIPlayer.GetComponentsInChildren<TMP_Text>();
                texts[0].text = count.ToString(); 
                texts[1].text = t.login;
                texts[2].text = t.best_score.ToString();
            }   
        }
        else
        {
            foreach (Transform child in UIRowParent.transform)
            {
                Destroy(child.gameObject);
            }
            int count = 0;
            foreach (var t in p.leaderboard)
            {
                count++;
                GameObject newUIPlayer = Instantiate(UIRowPrefab, UIRowParent);
                TMP_Text[] texts = newUIPlayer.GetComponentsInChildren<TMP_Text>();
                texts[0].text = count.ToString(); 
                texts[1].text = t.login;
                texts[2].text = t.best_score.ToString();
            }   
        }
    }

    
    public void SpawnPlayerUIPrefab(Player player) {
        GameObject clone;
        clone = UIPlayerParent.gameObject;
        Player.localPlayer.SetAvatarList();
        Debug.Log("Clone : " + clone.gameObject);

        GameObject newUIPlayer = Instantiate(UIPlayerPrefab.gameObject, clone.transform);
        newUIPlayer.GetComponent<UIPlayer>().SetPlayer(player);

        PlayerUIList.Add(newUIPlayer);

        //Pour que les joueurs s'affichent dans le bon ordre dans le lobby
        newUIPlayer.transform.SetSiblingIndex(player.playerIndex - 1);
    }

    public void DisbandLobby() {
        GameObject.FindWithTag("SonClick").GetComponent<AudioSource>().Play();
        Debug.Log("Je CLIQUE");
        Debug.Log("Waiting BORDEL ! : " + Player.localPlayer.WaitingTMP.Length);
        Player.localPlayer.DisbandLobby();
    }
    
    public void DisbandLobbyChangeUI() {
        //On détruit d'abord les objets avant de vider la liste
        foreach (GameObject player in PlayerUIList)
            Destroy(player);
        PlayerUIList.Clear();

        //Rendre intéragissables les boutons du menu
        hostButton.interactable = true;
        joinButton.interactable = true;

        //Cacher les boutons et le canva du lobby
        startGame_button.gameObject.SetActive(false);
        disbandLobby_button.gameObject.SetActive(false);
        lobbyCanvas.enabled = false;
        
        //Réactivation du menu
        mainMenu.SetActive(true);
    }
    
    public void SetMenuActive()
    {
        lobbyCanvas.enabled = false;
        lobbyUI.SetActive(true);
        mainMenu.SetActive(true);
    }
    
    public void StartGame() {
        GameObject.FindWithTag("SonClick").GetComponent<AudioSource>().Play();
        Player.localPlayer.StartGame();
        startGame_button.interactable = false;
        disbandLobby_button.interactable = false;
    }
}
