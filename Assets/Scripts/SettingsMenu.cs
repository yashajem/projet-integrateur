using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Mirror;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Wilberforce;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer audioMixer;
    public AudioSource click;

    public Camera camera;
    
    private Resolution[] _resolutions;
    public TMP_Dropdown resolutionDropdown;
    public TMP_Dropdown qualityDropdown;

    private int screenInt;
    private int dalto1Int;
    private int dalto2Int;
    private int dalto3Int;
    private int clickState;
    
    public Slider volslider;
    
    public GameObject PageLogin;
    public GameObject PageOption;
    public GameObject PageMDP;
    public GameObject PageInscription;
    public GameObject PageAide;
    public GameObject dernierePage;
    public Canvas Lobby;
    public GameObject PageMainMenu;
    public GameObject PageReintialiserMDP;

    public Button aideButton;
    public Button retourButton;
    public Button optionsButton;
    public Button quitButton;
    public Button mdpOublie;

    private bool isFullscreen = false;
    private bool dalto1Toggled = false;
    private bool dalto2Toggled = false;
    private bool dalto3Toggled = false;
    private bool isEnabled = false;

    public Toggle fullScreenToggle;
    public Toggle dalto1Toggle; 
    public Toggle dalto2Toggle;
    public Toggle dalto3Toggle;
    public Toggle sonToggle;
    
    public Toggle[] tg;

    private const string resName = "resolutionoption";
    private const string prefName = "optionvalue";

    private void Awake()
    {
        click = GameObject.FindGameObjectWithTag("SonClick").GetComponent<AudioSource>();

        
        screenInt = PlayerPrefs.GetInt("togglestate");
        clickState = PlayerPrefs.GetInt("clickState");
        dalto1Int = PlayerPrefs.GetInt("dalto1State");
        dalto2Int = PlayerPrefs.GetInt("dalto2State");
        dalto3Int = PlayerPrefs.GetInt("dalto3State");

        if (clickState == 0)
        {
            isEnabled = true;
            sonToggle.isOn = true;
        }
        else sonToggle.isOn = false;
        
        if (screenInt == 1)
        {
            isFullscreen = true;
            fullScreenToggle.isOn = true;
        }
        else
        {
            fullScreenToggle.isOn = false;
        }

        if (dalto1Int == 1)
        {
            dalto1Toggled = true;
            dalto1Toggle.isOn = true;
        }
        else
        {
            dalto1Toggle.isOn = false;
        }
        
        if (dalto2Int == 1)
        {
            dalto2Toggled = true;
            dalto2Toggle.isOn = true;
        }
        else
        {
            dalto2Toggle.isOn = false;
        }
        
        if (dalto3Int == 1)
        {
            dalto3Toggled = true;
            dalto3Toggle.isOn = true; 
        }
        else
        {
            dalto3Toggle.isOn = false;
        }
        
        resolutionDropdown.onValueChanged.AddListener(index =>
        {
            click.Play();
            PlayerPrefs.SetInt(resName, resolutionDropdown.value);
            PlayerPrefs.Save();
        });
        qualityDropdown.onValueChanged.AddListener(index =>
        {
            click.Play();
            PlayerPrefs.SetInt(prefName, qualityDropdown.value);
            PlayerPrefs.Save();
        });
    }

    private void Start()
    {
        volslider.value = PlayerPrefs.GetFloat("volume", 1f);
        audioMixer.SetFloat("volume", PlayerPrefs.GetFloat("volume"));
        qualityDropdown.value = PlayerPrefs.GetInt(prefName, 3);
        
        camera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        
        GetComponent<Canvas>().worldCamera = camera;

        _resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();
        resolutionDropdown.ClearOptions();

        int currentResolutionIndex = 0;
        
        List<string> options = new List<string>();
        for (int i = 0; i < _resolutions.Length; i++)
        {
            string option = _resolutions[i].width + " x " + _resolutions[i].height;
            options.Add(option);

            if (_resolutions[i].width == Screen.currentResolution.width 
                && _resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }
        
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = PlayerPrefs.GetInt(resName, currentResolutionIndex);
        resolutionDropdown.RefreshShownValue();
    }

    public void SetResolution(int resolutionIndex)
    {
        click.Play();
        Resolution res = _resolutions[resolutionIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
    }
    
    public void SetVolume(float volume)
    {
        PlayerPrefs.SetFloat("volume", volume);
        audioMixer.SetFloat("volume", PlayerPrefs.GetFloat("volume", volume));
        PlayerPrefs.Save();
    }

    public void SetQuality(int index)
    {
        click.Play();
        QualitySettings.SetQualityLevel(index);
    }

    public void SetFullScreen(bool isFullscreen)
    {
        click.Play();
        Screen.fullScreen = isFullscreen;

        if (isFullscreen == false)
        {
            PlayerPrefs.SetInt("togglestate", 0);
        }
        else
        {
            isFullscreen = true;
            PlayerPrefs.SetInt("togglestate", 1);
        }

        PlayerPrefs.Save();
    }

    public void SetDeuteranopie(bool dalto1Toggled)
    {
        click.Play();
        if (dalto1Toggled == false)
        {
            camera.GetComponent<Colorblind>().Type = 0;
            PlayerPrefs.SetInt("dalto1State", 0);
            
            foreach(var t in tg)
            {
                t.interactable = true;
            }
        }
        else
        {
            dalto1Toggled = true;
            camera.GetComponent<Colorblind>().Type = 2;
            PlayerPrefs.SetInt("dalto1State", 1);
            PlayerPrefs.SetInt("dalto2State", 0);
            PlayerPrefs.SetInt("dalto3State", 0);
            foreach(var t in tg)
            {
                if (!t.isOn)
                {
                    t.interactable = false;
                }
            }
        }
        PlayerPrefs.Save();
    }
    
    public void SetProta(bool dalto2Toggled)
    {
        click.Play();
        if (dalto2Toggled == false)
        {
            camera.GetComponent<Colorblind>().Type = 0;
            PlayerPrefs.SetInt("dalto2State", 0);
            foreach(var t in tg)
            {
                t.interactable = true;
            }
        }
        else
        {
            dalto2Toggled = true;
            camera.GetComponent<Colorblind>().Type = 1;
            PlayerPrefs.SetInt("dalto1State", 0);
            PlayerPrefs.SetInt("dalto2State", 1);
            PlayerPrefs.SetInt("dalto3State", 0);
            foreach(var t in tg)
            {
                if (!t.isOn)
                {
                    t.interactable = false;
                }
            }
        }
        PlayerPrefs.Save();
    }
    
    public void SetTritanopie(bool dalto3Toggled)
    {
        click.Play();
        if (dalto3Toggled == false)
        {
            camera.GetComponent<Colorblind>().Type = 0;
            PlayerPrefs.SetInt("dalto3State", 0);
            foreach(var t in tg)
            {
                t.interactable = true;
            }
        }
        else
        {
            dalto3Toggled = true;
            camera.GetComponent<Colorblind>().Type = 3;
            PlayerPrefs.SetInt("dalto3State", 1);
            PlayerPrefs.SetInt("dalto1State", 0);
            PlayerPrefs.SetInt("dalto2State", 0);
            foreach(var t in tg)
            {
                if (!t.isOn)
                {
                    t.interactable = false;
                }
            }
        }
        PlayerPrefs.Save();
    }

    public void CreerCompte()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Offline")
        {
            dernierePage = PageInscription;
            PageInscription.SetActive(true);
            PageLogin.SetActive(false);
            optionsButton.gameObject.SetActive(true);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);   
        }
    }
    
    public void Option()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Offline")
        {
            if (dernierePage == PageInscription)
            {
                Debug.Log("IN OPTION PAGE == INSCRIPTION");
            }

            if (dernierePage == PageMDP)
            {
                Debug.Log("IN OPTION PAGE == MDP");
            }

            PageOption.SetActive(true);
            PageLogin.SetActive(false);
            PageInscription.SetActive(false);
            PageMDP.SetActive(false);
            optionsButton.gameObject.SetActive(false);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);   
            aideButton.gameObject.SetActive(false);
        }

        if (SceneManager.GetActiveScene().name == "Lobby")
        {
            if (Lobby.enabled == true)
            {
                Lobby.gameObject.SetActive(false);
                //Lobby.enabled = false;
                PageOption.SetActive(true);
                optionsButton.gameObject.SetActive(false);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(false);
            }
            else if (PageMainMenu.activeSelf)
            {
                PageOption.SetActive(true);
                //PageMainMenu.SetActive(false);
                optionsButton.gameObject.SetActive(false);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);   
                aideButton.gameObject.SetActive(false);
            }
        }

        if (SceneManager.GetActiveScene().name == "Game")
        {
            camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            camera.transform.position = new Vector3(200, 200, 200);
            PageOption.SetActive(true);
            optionsButton.gameObject.SetActive(false);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);
            aideButton.gameObject.SetActive(false);
        }
    }

    public void Aides()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Offline")
        {
            PageAide.SetActive(true);
            PageLogin.SetActive(false);
            optionsButton.gameObject.SetActive(false);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);   
            aideButton.gameObject.SetActive(false);
        }

        if (SceneManager.GetActiveScene().name == "Lobby")
        {
            if (Lobby.enabled == true)
            {
                Lobby.gameObject.SetActive(false);
                //Lobby.enabled = false;
                PageAide.SetActive(true);
                optionsButton.gameObject.SetActive(false);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(false);
            }
            else if (PageMainMenu.activeSelf)
            {
                PageAide.SetActive(true);
                //PageMainMenu.SetActive(false);
                optionsButton.gameObject.SetActive(false);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);   
                aideButton.gameObject.SetActive(false);
            }
        }
        
        if (SceneManager.GetActiveScene().name == "Game")
        {
            camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            camera.transform.position = new Vector3(200, 200, 200);
            PageAide.SetActive(true);
            optionsButton.gameObject.SetActive(false);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);
            aideButton.gameObject.SetActive(false);
        }
    }
    
    public void Retour()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Offline")
        {
            if (PageAide.activeSelf)
            {
                PageAide.SetActive(false);
                PageLogin.SetActive(true);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(true);
                aideButton.gameObject.SetActive(true);
            }
            
            if (PageOption.activeSelf && dernierePage == PageMDP)
            {
                PageOption.SetActive(false);
                PageMDP.SetActive(true);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(true);
                return;
            }

            if (PageReintialiserMDP.activeSelf)
            {
                PageReintialiserMDP.SetActive(false);
                PageLogin.SetActive(true);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(true);
                aideButton.gameObject.SetActive(true);
            }

            if (PageOption.activeSelf && dernierePage == PageInscription)
            {
                PageOption.SetActive(false);
                PageInscription.SetActive(true);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(true);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(false);
            }
            else
            {
                PageOption.SetActive(false);
                PageLogin.SetActive(true);
                dernierePage = null;
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(true);
                aideButton.gameObject.SetActive(true);
            }

            if (PageInscription.activeSelf && dernierePage == null)
            {
                PageInscription.SetActive(false);
                PageLogin.SetActive(true);
                dernierePage = null;
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(true);
                aideButton.gameObject.SetActive(true);
            }

            if (PageMDP.activeSelf && dernierePage == null)
            {
                PageMDP.SetActive(false);
                PageLogin.SetActive(true);
                dernierePage = null;
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(true);
                aideButton.gameObject.SetActive(true);
            }
        }

        if (SceneManager.GetActiveScene().name == "Lobby")
        {
            if (PageOption.activeSelf)
            {
                if (!Lobby.gameObject.activeSelf)
                {
                    Lobby.gameObject.SetActive(true);
                    PageOption.SetActive(false);
                    optionsButton.gameObject.SetActive(true);
                    retourButton.gameObject.SetActive(true);
                    quitButton.gameObject.SetActive(false);   
                    aideButton.gameObject.SetActive(true);
                }
                else
                {
                    PageOption.SetActive(false);
                    optionsButton.gameObject.SetActive(true);
                    retourButton.gameObject.SetActive(false);
                    quitButton.gameObject.SetActive(true);   
                    aideButton.gameObject.SetActive(true);   
                }
            }

            if (PageAide.activeSelf)
            {
                if (!Lobby.gameObject.activeSelf)
                {
                    Lobby.gameObject.SetActive(true);
                    PageAide.SetActive(false);
                    optionsButton.gameObject.SetActive(true);
                    retourButton.gameObject.SetActive(true);
                    quitButton.gameObject.SetActive(false);   
                    aideButton.gameObject.SetActive(true);
                }
                else
                {
                    PageAide.SetActive(false);
                    optionsButton.gameObject.SetActive(true);
                    retourButton.gameObject.SetActive(false);
                    quitButton.gameObject.SetActive(true);   
                    aideButton.gameObject.SetActive(true);   
                }
            }
        }

        if (SceneManager.GetActiveScene().name == "Game")
        {
            if (PageOption.activeSelf)
            {
                camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
                camera.transform.position = new Vector3(0, 0, -10);
                PageOption.SetActive(false);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(true);
            }

            if (PageAide.activeSelf)
            {
                camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
                camera.transform.position = new Vector3(0, 0, -10);
                PageAide.SetActive(false);
                optionsButton.gameObject.SetActive(true);
                retourButton.gameObject.SetActive(false);
                quitButton.gameObject.SetActive(false);
                aideButton.gameObject.SetActive(true);
            }
        }
    }

    IEnumerator DisconnectThenQuit()
    {
        yield return new WaitForSeconds(5f);
        NetworkClient.connection.Disconnect();
        Application.Quit();
    }

    public void Quit()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Lobby" || SceneManager.GetActiveScene().name == "Game")
        {
            NetworkClient.connection.identity.GetComponent<Player>().Disconnect();
            StartCoroutine(DisconnectThenQuit());
        }
        else
        {
            Application.Quit();   
        }
    }

    public void MDPOublie()
    {
        click.Play();
        if (SceneManager.GetActiveScene().name == "Offline")
        {
            dernierePage = PageMDP;
            PageOption.SetActive(false);
            PageLogin.SetActive(false);
            PageMDP.SetActive(true);
            optionsButton.gameObject.SetActive(true);
            retourButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(false);   
        }
    }
    public void SetClickSound(bool isEnabled)
    {
        click.Play();

        if (isEnabled == false)
        {
            click.volume = 0.5f;
            PlayerPrefs.SetInt("clickState", 0);
        }
        else
        {
            click.volume = 0;
            PlayerPrefs.SetInt("clickState", 1);
        }

        PlayerPrefs.Save();
    }
}
