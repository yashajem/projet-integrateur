using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonHandler : MonoBehaviour
{
    public TMP_Text inputText;

    public void OnClick()
    {
        string response = EventSystem.current.currentSelectedGameObject
            .transform.GetChild(0).GetComponent<TMP_Text>().text;
        Debug.Log("Clicked : "+ response);
        inputText.text += response;
    }

    public void Effacer()
    {
        // Get the current text in the input field
        string text = inputText.text;

        // Erase the last character
        if (text.Length > 0)
        {
            text = text.Substring(0, text.Length - 1);
        }

        // Update the input field with the new text
        inputText.text = text;
 
    }
}
