using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private SpriteRenderer image;

    private Vector3 resetPosition;
    private Vector3 Difference;
    private Vector3 Origin;

    [SerializeField]
    private float ZoomChange, Smoothness, MinSize, MaxSize;

    [SerializeField]private bool drag = false;
    
    
    private void Start()
    {
        resetPosition = cam.transform.position;
    }

    private void LateUpdate()
    {
        if (Input.GetMouseButton(0))
        {
            Difference = (cam.ScreenToWorldPoint(Input.mousePosition)) - cam.transform.position;
            if (drag == false)
            {
                drag = true;
                Origin = cam.ScreenToWorldPoint(Input.mousePosition);
            }
        }
        else drag = false;
        
        
        if (drag)
        {
            float imageWidth = image.bounds.size.x;
            float imageHeight = image.bounds.size.y;
            float cameraWidth = cam.orthographicSize * 2 * cam.aspect;
            float cameraHeight = cam.orthographicSize * 2;

            float xMin = Mathf.Max(image.transform.position.x - imageWidth / 2 + cameraWidth / 2, cam.transform.position.x - cameraWidth / 2);
            float xMax = Mathf.Min(image.transform.position.x + imageWidth / 2 - cameraWidth / 2, cam.transform.position.x + cameraWidth / 2);
            float yMin = Mathf.Max(image.transform.position.y - imageHeight / 2 + cameraHeight / 2, cam.transform.position.y - cameraHeight / 2);
            float yMax = Mathf.Min(image.transform.position.y + imageHeight / 2 - cameraHeight / 2, cam.transform.position.y + cameraHeight / 2);

            cam.transform.position = Origin - Difference;
            cam.transform.position = new Vector3(
                Mathf.Clamp(cam.transform.position.x, xMin, xMax),
                Mathf.Clamp(cam.transform.position.y, yMin, yMax),
                cam.transform.position.z);
        }

        /*if (drag)
        {
            cam.transform.position = Origin - Difference;
            cam.transform.position = new Vector3(
                Mathf.Clamp(cam.transform.position.x, 0, 0),
                Mathf.Clamp(cam.transform.position.y, 0, 0), 
                transform.position.z);
        }*/

        if (Input.GetKeyDown(KeyCode.R)) cam.transform.position = resetPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.mouseScrollDelta.y > 0) cam.orthographicSize -= ZoomChange * Time.deltaTime * Smoothness;
        if (Input.mouseScrollDelta.y < 0) cam.orthographicSize += ZoomChange * Time.deltaTime * Smoothness;

        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, MinSize, MaxSize);
    }
}
