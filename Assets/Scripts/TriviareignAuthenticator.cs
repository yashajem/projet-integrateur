using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Mirror;
using TMPro;
using UnityEngine.Serialization;


[AddComponentMenu("Network/ Authenticators/Triviareign Authenticator")]
public class TriviareignAuthenticator : NetworkAuthenticator
{
    [Header("Login")]
    public TMP_InputField user;
    public TMP_InputField pwd;
    
    [Header("Inscription")]
    public TMP_InputField inscr_usr;
    public TMP_InputField inscr_pwd;
    public TMP_InputField inscr_mail;

    [Header("MDP Oublie")] 
    public TMP_InputField oublie_mail;

    [Header("Reset Password")] 
    public TMP_InputField usernameReset;
    public TMP_InputField codeReset;
    public TMP_InputField passReset;
    public TMP_InputField passVerifReset;


    public GameObject PageReinitialiserMDP;
    public GameObject PageLogin;
    public GameObject PageMDPOublie;

    public TMP_Text messageErreur;
    public TMP_Text messageErreurMDPOublie;
    public TMP_Text messageErreurReset;

    
    [Header("Client Credentials")]
    public string username;
    public string password;
    public string passwordVerif;
    public string email;
    public string code;

    [SerializeField]
    public bool login;
    
    [SerializeField]
    public bool reg;

    [SerializeField] public bool forgot;

    [SerializeField] public bool resetPass;
    
    readonly HashSet<NetworkConnection> connectionsPendingDisconnect = new HashSet<NetworkConnection>();

    #region Messages

    public struct AuthRequestMessage : NetworkMessage
    {
        // use whatever credentials make sense for your game
        // for example, you might want to pass the accessToken if using oauth
        public bool authReg;
        public bool authLog;
        public bool authForgot;
        public bool authReset;
        public string authUsername;
        public string authPassword;
        public string authPasswordVerif;
        public string authEmail;
        public string authCode;
    }

    public struct AuthResponseMessage : NetworkMessage
    {
        public int code;
        public string message;
    }

    #endregion

    #region Server

    /// <summary>
    /// Called on server from StartServer to initialize the Authenticator
    /// <para>Server message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStartServer()
    {
        // register a handler for the authentication request we expect from client
        NetworkServer.RegisterHandler<AuthRequestMessage>(OnAuthRequestMessage, false);
    }

    /// <summary>
    /// Called on server from StopServer to reset the Authenticator
    /// <para>Server message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStopServer()
    {
        // unregister the handler for the authentication request
        NetworkServer.UnregisterHandler<AuthRequestMessage>();
    }

    /// <summary>
    /// Called on server from OnServerConnectInternal when a client needs to authenticate
    /// </summary>
    /// <param name="conn">Connection to client.</param>
    public override void OnServerAuthenticate(NetworkConnectionToClient conn)
    {
        // do nothing...wait for AuthRequestMessage from client
    }

    IEnumerator SendMdpOublie(string url, string email, NetworkConnectionToClient conn)
    {
        Debug.Log(email);
        
        WWWForm form = new WWWForm();
        form.AddField("email",email);

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            Debug.Log("Dans webRequest");
            
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
                connectionsPendingDisconnect.Add(conn);
                StartCoroutine(DelayedDisconnect(conn, 1f));
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                Debug.Log(response);

                switch (response)
                {
                    case 201:
                    {
                        // create and send msg to client so it knows to proceed
                        AuthResponseMessage authResponseMessage = new AuthResponseMessage
                        {
                            code = 201,
                            message = "Success"
                        };

                        conn.Send(authResponseMessage);

                        connectionsPendingDisconnect.Add(conn);
                        StartCoroutine(DelayedDisconnect(conn, 1f));
                        break;
                    }
                    case 400:
                    {
                        Debug.Log("Connection failed");
                        connectionsPendingDisconnect.Add(conn);

                        // create and send msg to client so it knows to disconnect
                        AuthResponseMessage authResponseMessage = new AuthResponseMessage
                        {
                            code = 400,
                            message = "Invalid Credentials"
                        };

                        conn.Send(authResponseMessage);

                        // must set NetworkConnection isAuthenticated = false
                        conn.isAuthenticated = false;

                        // disconnect the client after 1 second so that response message gets delivered
                        StartCoroutine(DelayedDisconnect(conn, 1f));
                        break;
                    }
                    case 401:
                    {
                        //Champs vide
                        Debug.Log("Le mail n'existe pas dans la BDD");
                        connectionsPendingDisconnect.Add(conn);

                        // create and send msg to client so it knows to disconnect
                        AuthResponseMessage authResponseMessage = new AuthResponseMessage
                        {
                            code = 3,
                            message = "Le mail n'existe pas dans la BDD"
                        };

                        conn.Send(authResponseMessage);

                        // must set NetworkConnection isAuthenticated = false
                        conn.isAuthenticated = false;

                        // disconnect the client after 1 second so that response message gets delivered
                        StartCoroutine(DelayedDisconnect(conn, 1f));
                        break;
                    }
                    case 402:
                    {
                        //Presence serveur == 1, le joueur ne peut pas se connecter sur le serveur car il est deja login
                        Debug.Log("Le mail n'est pas envoyé");
                        connectionsPendingDisconnect.Add(conn);

                        // create and send msg to client so it knows to disconnect
                        AuthResponseMessage authResponseMessage = new AuthResponseMessage
                        {
                            code = 4,
                            message = "Le mail n'est pas envoyé"
                        };
                    
                    
                        conn.Send(authResponseMessage);

                        // must set NetworkConnection isAuthenticated = false
                        conn.isAuthenticated = false;

                        // disconnect the client after 1 second so that response message gets delivered
                        StartCoroutine(DelayedDisconnect(conn, 1f));
                        break;
                    }
                }
            }
        }
    }

    IEnumerator SendResetRequest(string url, string username, string code, string password, string passwordVerif,
        NetworkConnectionToClient conn)
    {
        WWWForm form = new WWWForm();
        
        form.AddField("username", username);
        form.AddField("code", code);
        form.AddField("new_password", password);
        form.AddField("verif_pass", passwordVerif);
        
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
                connectionsPendingDisconnect.Add(conn);
                StartCoroutine(DelayedDisconnect(conn, 1f));
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 401)
                {
                    connectionsPendingDisconnect.Add(conn);
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage()
                    {
                        code = 401,
                        message = "User does not exist"
                    };
                    
                    conn.Send(authResponseMessage);
                    conn.isAuthenticated = false;
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }

                if (response == 402)
                {
                    connectionsPendingDisconnect.Add(conn);
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage()
                    {
                        code = 5,
                        message = "Wrong code"
                    };
                    
                    conn.Send(authResponseMessage);
                    conn.isAuthenticated = false;
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                
                if (response == 400)
                {
                    Debug.Log("Connection failed");
                    connectionsPendingDisconnect.Add(conn);

                    // create and send msg to client so it knows to disconnect
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 400,
                        message = "UPDATE failed"
                    };

                    conn.Send(authResponseMessage);

                    // must set NetworkConnection isAuthenticated = false
                    conn.isAuthenticated = false;

                    // disconnect the client after 1 second so that response message gets delivered
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                if (response == 200)
                {
                    // create and send msg to client so it knows to proceed
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 202,
                        message = "Success"
                    };
                    
                    conn.Send(authResponseMessage);
                }
            }
        }
    }
    
    IEnumerator SendLoginPostRequest(string url, string username, string password, NetworkConnectionToClient conn)
    {
        Debug.Log("USR + PWD : " + username + " " + password);
        
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("id_conn", conn.connectionId);
        
        
        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
                connectionsPendingDisconnect.Add(conn);
                StartCoroutine(DelayedDisconnect(conn, 1f));
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);
                
                if (response == 400)
                {
                    Debug.Log("Connection failed");
                    connectionsPendingDisconnect.Add(conn);

                    // create and send msg to client so it knows to disconnect
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 2,
                        message = "Invalid Credentials"
                    };

                    conn.Send(authResponseMessage);

                    // must set NetworkConnection isAuthenticated = false
                    conn.isAuthenticated = false;

                    // disconnect the client after 1 second so that response message gets delivered
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                
                if (response == 402)
                {
                    //Presence serveur == 1, le joueur ne peut pas se connecter sur le serveur car il est deja login
                    Debug.Log("Le joueur est deja sur le serveur");
                    connectionsPendingDisconnect.Add(conn);

                    // create and send msg to client so it knows to disconnect
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 1,
                        message = "Session deja active"
                    };
                    
                    
                    conn.Send(authResponseMessage);

                    // must set NetworkConnection isAuthenticated = false
                    conn.isAuthenticated = false;

                    // disconnect the client after 1 second so that response message gets delivered
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }

                if (response == 403)
                {
                    //Update presence serveur echoue
                    Debug.Log("Update presence echoué");
                    connectionsPendingDisconnect.Add(conn);

                    // create and send msg to client so it knows to disconnect
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 403,
                        message = "Une erreur est apparu. Veuillez re-essayer"
                    };

                    conn.Send(authResponseMessage);

                    // must set NetworkConnection isAuthenticated = false
                    conn.isAuthenticated = false;

                    // disconnect the client after 1 second so that response message gets delivered
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                
                
                if (response == 200)
                {
                   
                    // create and send msg to client so it knows to proceed
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 200,
                        message = "Success"
                    };

                    conn.Send(authResponseMessage);

                    // Accept the successful authentication
                    ServerAccept(conn);
                }
            }
        }
    }
 /******/
 /*Anyas */
    IEnumerator SendInformationInscription(string url, string username, string email, string password, NetworkConnectionToClient conn)
    {
        Debug.Log("USR + Mail + PWD : " + username + " " + email + " " + password);
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("email", email);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log("Post request error");
                connectionsPendingDisconnect.Add(conn);
                StartCoroutine(DelayedDisconnect(conn, 1f));
            }
            else
            {
                int response = int.Parse(www.downloadHandler.text);

                if (response == 401)
                {
                    connectionsPendingDisconnect.Add(conn);
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage()
                    {
                        code = 401,
                        message = "Insertion error"
                    };
                    
                    conn.Send(authResponseMessage);
                    conn.isAuthenticated = false;
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }

                if (response == 402)
                {
                    connectionsPendingDisconnect.Add(conn);
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage()
                    {
                        code = 402,
                        message = "Vous avez saisi des champs vides"
                    };
                    
                    conn.Send(authResponseMessage);
                    conn.isAuthenticated = false;
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                
                if (response == 400)
                {
                    Debug.Log("Connection failed");
                    connectionsPendingDisconnect.Add(conn);

                    // create and send msg to client so it knows to disconnect
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 400,
                        message = "Invalid Credentials"
                    };

                    conn.Send(authResponseMessage);

                    // must set NetworkConnection isAuthenticated = false
                    conn.isAuthenticated = false;

                    // disconnect the client after 1 second so that response message gets delivered
                    StartCoroutine(DelayedDisconnect(conn, 1f));
                }
                if (response == 200)
                {
                    // create and send msg to client so it knows to proceed
                    AuthResponseMessage authResponseMessage = new AuthResponseMessage
                    {
                        code = 200,
                        message = "Success"
                    };

                    conn.Send(authResponseMessage);

                    // Accept the successful authentication
                    ServerAccept(conn);
                }
            }
        }
    }
    /*******/

    /// <summary>
    /// Called on server when the client's AuthRequestMessage arrives
    /// </summary>
    /// <param name="conn">Connection to client.</param>
    /// <param name="msg">The message payload</param>
    public void OnAuthRequestMessage(NetworkConnectionToClient conn, AuthRequestMessage msg)
    {
        //Debug.Log($"Authentication Request: {msg.authUsername} {msg.authPassword}");

        if (connectionsPendingDisconnect.Contains(conn)) return;
        string url = "192.168.100.104/login_conn.php";
        string url1="192.168.100.104/register_sec.php";
        string url_forgot = "192.168.100.104/test.php";
        string url_reset = "192.168.100.104/reset_password.php";
        if (msg.authLog && msg.authReg == false && msg.authForgot == false && msg.authReset == false) 
            StartCoroutine(SendLoginPostRequest(url, msg.authUsername, msg.authPassword, conn));
        else if (msg.authReg && msg.authLog == false && msg.authForgot == false && msg.authReset == false)
            StartCoroutine(SendInformationInscription(url1, msg.authUsername, msg.authEmail, msg.authPassword, conn));
        else if (msg.authForgot && msg.authLog == false && msg.authReg == false && msg.authReset == false)
            StartCoroutine(SendMdpOublie(url_forgot, msg.authEmail, conn));
        else
            StartCoroutine(SendResetRequest(url_reset, msg.authUsername, msg.authCode, msg.authPassword, msg.authPasswordVerif, conn));
    }
    /****/
    
    IEnumerator DelayedDisconnect(NetworkConnectionToClient conn, float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        // Reject the unsuccessful authentication
        ServerReject(conn);

        yield return null;

        // remove conn from pending connections
        connectionsPendingDisconnect.Remove(conn);
    }

    #endregion

    #region Client

    /// <summary>
    /// Called on client from StartClient to initialize the Authenticator
    /// <para>Client message handlers should be registered in this method.</para>
    /// </summary>
    public override void OnStartClient()
    {
        if (login)
        {
            username = user.text;
            password = pwd.text;   
        }
        if (reg)
        {
            username = inscr_usr.text;
            password = inscr_pwd.text;
            email = inscr_mail.text;
        }

        if (forgot)
        {
            email = oublie_mail.text;
        }

        if (resetPass)
        {
            username = usernameReset.text;
            code = codeReset.text;
            password = passReset.text;
            passwordVerif = passVerifReset.text;
        }

        // register a handler for the authentication response we expect from server
        NetworkClient.RegisterHandler<AuthResponseMessage>(OnAuthResponseMessage, false);
    }

    /// <summary>
    /// Called on client from StopClient to reset the Authenticator
    /// <para>Client message handlers should be unregistered in this method.</para>
    /// </summary>
    public override void OnStopClient()
    {
        // unregister the handler for the authentication response
        NetworkClient.UnregisterHandler<AuthResponseMessage>();
    }

    /// <summary>
    /// Called on client from OnClientConnectInternal when a client needs to authenticate
    /// </summary>
    public override void OnClientAuthenticate()
    {
        if (login)
        {
            AuthRequestMessage authRequestMessage = new AuthRequestMessage
            {
                authUsername = username,
                authPassword = password,
                authLog = login,
                authReg = false,
                authReset = false,
                authForgot = false
            };
            NetworkClient.Send(authRequestMessage);
            login = false;
            return;
        }
        
        if (reg)
        {
            AuthRequestMessage authRequestMessage = new AuthRequestMessage
            {
                authUsername = username,
                authPassword = password,
                authEmail = email,
                authReg = reg,
                authReset = false,
                authLog = false,
                authForgot = false
            };
            NetworkClient.Send(authRequestMessage);
            reg = false;
            return;
        }

        if (forgot)
        {
            AuthRequestMessage authRequestMessage = new AuthRequestMessage
            {
                authEmail = email,
                authReg = false,
                authLog = false,
                authReset = false,
                authForgot = forgot
            };
            NetworkClient.Send(authRequestMessage);
            forgot = false;
            return;
        }

        if (resetPass)
        {
            AuthRequestMessage authRequestMessage = new AuthRequestMessage
            {
                authUsername = username,
                authCode = code,
                authPassword = password,
                authPasswordVerif = passwordVerif,
                authReg = false,
                authLog = false,
                authReset = resetPass,
                authForgot = false
            };
            NetworkClient.Send(authRequestMessage);
            resetPass = false;
        }
    }

    /// <summary>
    /// Called on client when the server's AuthResponseMessage arrives
    /// </summary>
    /// <param name="msg">The message payload</param>
    public void OnAuthResponseMessage(AuthResponseMessage msg)
    {
        #region Messages Erreur

        /* LOGIN */
        if (msg.code == 1)
        {
            messageErreur.text = "Session déjà active.";
        }
        
        if (msg.code == 2)
        {
            messageErreur.text = "Nom d'utilisateur ou mot de passe incorrect. Veuillez réessayer.";
        }

        //Reinitialisation mot de passe
        if (msg.code == 3)
        {
            messageErreurMDPOublie.text = "Vous avez saisi un mauvais email. Veuillez réessayer.";
        }
        
        if (msg.code == 4)
        {
            messageErreurMDPOublie.text = "Echéc de l'envoi du mail. Veuillez réessayer.";
        }

        if (msg.code == 5)
        {
            messageErreurReset.text = "Vous avez saisi le mauvais code. Veuillez réessayer.";
        }
        

        #endregion
        
        
        //Login
        if (msg.code == 201)
        {
            Debug.Log($"Authentication Response: {msg.message}");
            PageReinitialiserMDP.SetActive(true);
            GameObject.FindGameObjectWithTag("OptionButton").gameObject.SetActive(false);
            GameObject.FindGameObjectWithTag("AideButton").gameObject.SetActive(false);
            messageErreurReset.text = "Votre code a bien été envoyé. Veuillez vérifier votre mail.";
            messageErreurReset.color = new Color(0.28f, 1, 0.3f);
        }
        
        //ReinitialiserMDP
        if (msg.code == 202)
        {
            Debug.Log($"Authentication Response: {msg.message}");
            PageReinitialiserMDP.SetActive(false);
            PageMDPOublie.SetActive(false);
            PageLogin.SetActive(true);
        }
        
        if (msg.code == 200)
        {
            Debug.Log($"Authentication Response: {msg.message}");

            // Authentication has been accepted;

            ClientAccept();
        }
        else
        {
            Debug.LogError($"Authentication Response: {msg.message}");

            // Authentication has been rejected
            ClientReject();
        }
    }

    #endregion
}

