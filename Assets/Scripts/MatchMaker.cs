using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using System.Security.Cryptography;
using System.Text;
using Random = UnityEngine.Random;

[Serializable]
public class Match {
    public string matchID;
    public List<Player> players = new List<Player>();
    
    //Constructeur, playerHost est le joueur qui crée le nouveau match et on l'ajoute à la liste
    public Match(string matchID, Player playerHost) {
        this.matchID = matchID;
        players.Add(playerHost);
    }

    //Besoin d'un blank constructor pour que ce soit serializable ?
    public Match() {}
}


[Serializable]
//Liste tous les matchs qui ont lieu en parallèle
public class SyncListMatch : SyncList<Match> {

}

public class MatchMaker : NetworkBehaviour
{ 
    public static MatchMaker instance;
    public readonly SyncListMatch matches = new SyncListMatch();
    public readonly SyncList<string> matchIDs = new SyncList<string>();
    
    [SerializeField] GameObject turnManagerPrefab;
    [SerializeField] GameObject[] territories;
    private GameManager _gameManager;

    void Start() { 
        instance = this; 
    }

    //Static pour qu'on puisse y accéder dans Player.cs
    public static string GetRandomMatchId() {
        string idString = "";
        int random = UnityEngine.Random.Range(32, 122);
       
        idString = random.ToString();

        Debug.Log("Match ID : " + idString);
        return idString;
    }


    //out pe
    public bool HostGame(string matchID, Player player, out int playerIndex) {
        //On ajoute un nouveau match à la liste des matchs
        //Si cet ID n'est pas déjà dans la liste

        playerIndex = -1;
        if (!matchIDs.Contains(matchID)) {
                matchIDs.Add(matchID);
                matches.Add(new Match(matchID, player));
                Debug.Log("Match Generated");
                playerIndex = 1;
                return true; 
        }
        Debug.Log("Match ID already exists");
        return false;
    }

    //out permet de faire ref à une variable partout où cette fonction est appelée
    public bool JoinGame(string matchID, Player player, out int playerIndex) {
        playerIndex = -1;
        if (matchIDs.Contains(matchID)) {
            Debug.Log("Match joined");

            //On parcourt tous les match existant
            for (int i  = 0; i < matches.Count; i++) {
                //Quand on trouve notre match, on ajoute le joueur dedans et on break
                if (matches[i].matchID == matchID) {
                    matches[i].players.Add(player);
                    playerIndex = matches[i].players.Count;
                    break;
                }
            }
            return true;
        }
        Debug.Log("Match ID does not exist");
        return false;
        
    }

    /* ALEX */
    public void MatchInitializeMap(string matchID)
    {
        GameObject t;
        foreach (GameObject ter in territories)
        {   
            Debug.Log("Spawning : " + ter.name);
            t = Instantiate(ter, ter.GetComponent<Territory>().spawnPoint.transform.position, Quaternion.identity);
            NetworkServer.Spawn(t);
            t.GetComponent<NetworkMatch>().matchId = matchID.ToGuid();
        }
    }
    
    public void MatchUnloadMap(string matchID)
    {
        foreach (GameObject ter in territories)
        {
            if (ter.GetComponent<NetworkMatch>().matchId == matchID.ToGuid())
            {
                NetworkServer.Destroy(ter);
            }
        }
    }
    
    public void InitPlayerData(Player p)
    {
        //Au debut on a 1000 points
        p.points = 1000;
        
        //On attribue une couleur au un joueur
        p.playerColor = GameManager.instance.ServerAssignColor();
        
        //Mettre le premier tour au premier joueur dans la liste
        GameManager.instance.ServerSetFirstTurn();

        //ClientMessage sur le serveur
        Debug.Log("[INFO] Player data successfully initialized!");
    }
    
    public void StartGame(string matchID)
    {
        //On charge le GameManager sur le serveur
        GameObject newTurnManager = Instantiate(turnManagerPrefab);
        NetworkServer.Spawn(newTurnManager);
        newTurnManager.GetComponent<NetworkMatch>().matchId = matchID.ToGuid();
        _gameManager = newTurnManager.GetComponent<GameManager>();
        
        MatchInitializeMap(matchID);

        for (int i = 0; i < matches.Count; i++) {
            if (matches[i].matchID == matchID) {
                
                /*----------REMINDER----------*/
                //Faut pas oublier d'ajouter la precondition pour lancer un match
                //      On peut pas start une game si on a moins de 3 joueurs.  

                foreach (var player in matches[i].players) {
                    //GameManager aura la liste de tous ses joueurs
                    _gameManager.ServerAddPlayer(player);
                    //Initialiser les donnees d'un joueur
                    InitPlayerData(player);
                    //Sur le serveur on dit à chacun de ses joueurs d'appeler
                    player.BeginGame();
                }
                break;
            }
        }
    }

    /*ADEL*/
    public void DisbandLobby(string matchId) {
        int index = matchIDs.IndexOf(matchId);
        matchIDs.RemoveAt(index);
        matches.RemoveAt(index);
    }
    
    public void RemovePlayerFromMatch(Player p, string matchId) {
        int index = matchIDs.IndexOf(matchId);
        matches[index].players.Remove(p);
    }
}

 //wtf mais ok
    public static class MatchExtensions {
        public static Guid ToGuid (this string id) {
            MD5CryptoServiceProvider provider = new MD5CryptoServiceProvider();
            byte[] inputBytes = Encoding.Default.GetBytes(id);
            byte[] hashBytes = provider.ComputeHash(inputBytes);

            return new Guid(hashBytes);
        }
    }
