using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using Telepathy;
using UnityEngine;
using UnityEngine.Networking;

public struct TerritoryMessage : NetworkMessage
{
    public Player p;
    public Territory t;
    public bool territoryClicked;
    public bool QnA;
}

[RequireComponent(typeof(PolygonCollider2D))]
public class Territory : NetworkBehaviour
{
    [SyncVar(hook = nameof(OnIntChanged))]
    public int currentPhase;
    public GameObject spawnPoint;
    public Player player;
    [SyncVar(hook = nameof(OnBoolChange))]
    public bool captured;
    [SyncVar(hook = nameof(OnBoolBChange))]
    public bool isBase;
    [SyncVar(hook = nameof(OnBoolFChange))]
    public bool isFortified ;

    public static bool fortMode ;
    
    //Collider
    public PolygonCollider2D pc2d;
    
    //Neighbours of a certain territory
    public List<Territory> neighbours = new ();

    [SyncVar(hook = nameof(OnColorChange))]
    public Color territoryColor;

    private void Start()
    {
        /* Recuperation de collider - C'est pour la partie client */
        pc2d = GetComponent<PolygonCollider2D>();
        
        /* Recuperer la couleur d'un territoire */
        territoryColor = GetComponent<SpriteRenderer>().color;
        
        isFortified = false;

        fortMode = false ;
        
        /* Appel a fonction DetectNeighbours qui va detecter les territoires voisins de ce territoire */
        DetectNeighbours();
    }

    /* Detection des territoires voisins */
    void DetectNeighbours()
    {
        GameObject[] ter = GameObject.FindGameObjectsWithTag("Territory");
        
        for (int i = 0; i < ter.Length; i++)
        {
            if (ter[i].gameObject.GetInstanceID() != gameObject.GetInstanceID())
            {
                if (pc2d.bounds.Intersects(ter[i].GetComponent<Territory>().GetComponent<PolygonCollider2D>().bounds) && !neighbours.Contains(ter[i].GetComponent<Territory>()))
                {
                    neighbours.Add(ter[i].GetComponent<Territory>());
                }
            }
        }
    }

    /* Synchronization des couleurs */
    void OnColorChange(Color oldColor, Color newColor)
    {
        GetComponent<SpriteRenderer>().color = newColor;
        Debug.Log($"Color changed from {oldColor} to {newColor}");
    }
    
    void OnBoolFChange(bool old, bool newBool)
    {
        isFortified = newBool;
    }
    
    void OnBoolChange(bool old, bool newBool)
    {
        captured = newBool;
        Debug.Log($"Color changed from {old} to {newBool}");
    }

    void OnIntChanged(int old, int newPh)
    {
        currentPhase = newPh;
    }
    
    void OnBoolBChange(bool old, bool newBool)
    {
        isBase = newBool;
    }

    [Command(requiresAuthority = false)]
    void GetPhase()
    {
        if (GameManager.instance.gameStatus == GamePhase.baseSelect_1)
        {
            currentPhase = 1;
        }

        if (GameManager.instance.gameStatus == GamePhase.expantion_21)
        {
            currentPhase = 2;
        }

        if (GameManager.instance.gameStatus == GamePhase.battle_3)
        {
            currentPhase = 3;
        }
    }
    
    /* Quand le joueur va  */
    public void OnMouseEnter()
    {
        GetPhase();
        NetworkIdentity nId = NetworkClient.connection.identity;
        player = nId.transform.GetComponent<Player>();

        if (player.territoryList.Contains(this))
        {
            Debug.Log("That territory is mine");
            Color myTer = new Color(0.5f, 1.5f, 2, 0.5f);
            GetComponent<SpriteRenderer>().color = myTer;
        }
        else
        {
            if (captured)
            {
                Debug.Log("this is the territory of someone else");
                Color capturedTer = new Color(1.5f, 0.5f, 0.5f, 0.5f);
                GetComponent<SpriteRenderer>().color = capturedTer;   
            }
            else
            {
                Debug.Log("This territory is free!");
                Color freeTer = new Color(0.5f, 1.5f, 0.5f, 0.5f);
                GetComponent<SpriteRenderer>().color = freeTer;
            }
        }
    }

    public void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = territoryColor;
    }
    
    //Debug
    [Command(requiresAuthority = false)]
    void CmdShow(Player p)
    {
        Debug.Log(p + " " + p.myTurn + " " + p.playerColor + " " + p.points);
    }

    [Command(requiresAuthority = false)]
    void CmdSetTerritoryCaptured()
    {
        captured = true;
    }
    
    [Command(requiresAuthority = false)]
    void CmdSetTerritoryFortified()
    {
        isFortified = true;
    }
    
    public void SetBase()
    {
        isBase = true;
        isFortified = true;
    }


    [Command(requiresAuthority = false)]
    void CmdChangeFortifiedColor(Territory t, Color c, Player p)
    {
        GameManager.instance.ServerChangeColor(t, c, p);
    }
    
    public void OnMouseDown()
    {
        var capturedNeighbours = 0;
        
        if (isBase && currentPhase == 1)
        {
            Debug.Log("You cant do that!");
            return;
        }
        
        //On recupere le joueur qui a clique sur le territoire 
        NetworkIdentity nId = NetworkClient.connection.identity;
        player = nId.transform.GetComponent<Player>();
        
        //Fonction de debug
        CmdShow(player);

        foreach (var ter in player.neighbours)
        {
            if (ter is null)
            {
                player.neighbours.Remove(ter);
            }
            else if (ter.captured)
            {
                capturedNeighbours++;
            }
        }

        Debug.Log($"Captured neighbours : {capturedNeighbours}");

        //Si c'est son tour, on continue
        if (player.myTurn)
        {
            GameObject.FindGameObjectWithTag("TerritoryClickSound").GetComponent<AudioSource>().Play();
            
            if (!player.neighbours.Contains(this) && (currentPhase == 2 || currentPhase == 3) && !fortMode)
            {
                if (currentPhase == 3)
                {
                    Debug.Log("Tu peux pas cliquer la");
                    return;
                }
                
                if (capturedNeighbours == player.neighbours.Count && currentPhase == 2)
                {
                    goto Test;
                    return;
                }
                Debug.Log("Tu peux pas lol");
                return;
            }
            
            //Si le player a deja ce territoire, il peut pas le clicker
            if (player.territoryList.Contains(this))
            {
                if(fortMode && !isBase && currentPhase == 3)
                {
                    CmdSetTerritoryFortified();
                    UIGame.instance.fortificationButton.interactable = false;
                    Debug.Log("[FORT] Territoire fortifié! : " + isFortified);
                    CmdChangeFortifiedColor(this, territoryColor, player);
                }

                return;
            }
            
            //Si le territoire qu'il a clické contient un neighbour qui a "isBase == true",
            //on return disant que on peut pas faire ca
            if (currentPhase == 1)
            {
                foreach (var neighbour in this.neighbours)
                {
                    if (neighbour.isBase)
                    {
                        Debug.Log("Tu peux pas avoir une base adjacente a une autre");
                        return; 
                    }
                }
            }

            Test:
            {
                //phase d'expansion - ne pas toucher
                if (currentPhase == 2)
                {
                    if (captured)
                    {
                        Debug.Log("You cant , its captured already!");
                        return;
                    }
                    else
                    {
                        if (!captured)
                        {
                            CmdSetTerritoryCaptured();
                            player.CmdSelectTerritory(this);
                        }
                        else
                        {
                            CmdSetTerritoryCaptured();
                            player.CmdSelectTerritory(this);
                        }
                    } 
                }   
            }

            //Phase de bataille - ne pas toucher
            if (captured && currentPhase == 3)
            {
                player.isAtacking = true;
                player.CmdIsAttacking(true);
                NetworkClient.Send(new TerritoryMessage{
                    territoryClicked =  true, 
                    p = player, 
                    t = this,
                    QnA = true
                });
            }
            else
            {// selection de base - ne pas toucher
                //Capturer le territoire

                Debug.Log("[TERRITORY] Dans le dernier else");
                
                player.CmdCaptureTerritory(this);
            
                //Dire au serveur de update l'etat de jeu pour tous les joueurs
                player.CmdUpdateData(this);
                
                CmdSetTerritoryCaptured();
                
                //verifier si send echoue
                NetworkClient.Send(new TerritoryMessage{
                    territoryClicked =  true, 
                    p = player, 
                    t = this,
                    QnA = false
                });
            }
        }
        else
        {
            Debug.Log("Not your turn");
        }
    }
}
